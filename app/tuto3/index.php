<?php

$currentURL = $_SERVER['REQUEST_URI'];
$tutoURL = substr($currentURL, 0, strpos($currentURL, 'app/') - 1);
$lang = $_GET['lang'] ?: 'fr';
$arrayPages=explode("?",$currentURL);
//var_dump($arrayPages);
if(count($arrayPages)==2){
    $pageTuto = $arrayPages[count($arrayPages)-1];
}else{
    $pageTuto = "";
}
//echo "page tuto : ".$pageTuto;
?>

<!doctype html>
<html>
<head>
<meta charset='UTF-8'>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Class'Code IAI : Humains et IA !</title>
<link href='<?php echo $tutoURL; ?>/assets/css/tutos-ia.css' rel='stylesheet' type='text/css' />
    <link href='<?php echo $tutoURL; ?>/assets/css/tuto3.css' rel='stylesheet' type='text/css' />
    <link href='<?php echo $tutoURL; ?>/assets/css/tuto3-devices.css' rel='stylesheet' type='text/css' />
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700,800|Rambla:400,700&display=swap" rel="stylesheet" />
    <script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/libs/jquery-3.1.1.min.js'></script>
    <script type='text/javascript' src='https://pixees.fr/wp-content/plugins/google-analyticator/external-tracking.min.js?ver=6.5.4'></script>

</head>
<body >

    <div class='tuto-ia-application' data-baseURL="<?php echo $tutoURL; ?>" data-lang="<?php echo $lang; ?>">
        <div id='step-header'   class='step-header'></div>
        <div id='step-contents' class='step-contents'></div>
        <div id='step-footer'   class='step-footer'></div>
        <div class='popup hidden'></div>
    </div>

    <!-- templates ------>
    <script id='dataset-template' type='text/x-handlebars-template'>
        <ul data-datasetname='{{datasetname}}'>
            {{#each files}}
            <li class='dataset-image' style='background-image:url({{../datasetpath}}/{{path}})' >
                <img
                        src='{{../datasetpath}}/{{path}}'
                        class='hidden-dataset-image'
                        data-path='{{path}}'

                        {{#if datasetname}}
                        data-datasetname='{{datasetname}}'
                        {{else}}
                        data-datasetname='{{../datasetname}}'
                        {{/if}}
                />
            </li>
            {{/each}}
        </ul>
    </script>
    <div class="message-device" >
        <p>Cette partie n'est pas adaptés aux appareils mobiles.</p>
        <p>Veuillez la consulter dans Chrome sur un ordinateur.</p>
    </div>
    <script>
        let pageTuto3="<?php echo $pageTuto; ?>";
    </script>
	<!-- scripts ------>
    <script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/libs/encoding-indexes.js'></script>
    <script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/libs/encoding.js'></script>
    <script type='text/javascript' src='https://unpkg.com/ml5@0.4.2/dist/ml5.min.js'></script>
	<script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/libs/deparam.min.js'></script>
	<script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/libs/jquery.router.js'></script>
	<script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/libs/handlebars-v4.4.3.js'></script>
	<script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/libs/handlebars_utils.js'></script>
    <script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/libs/i18n.min.js'></script>
    <script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/libs/tutos_utils.js'></script>
    <script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/tuto3-devices.js'></script>
    <script type='text/javascript' src='<?php echo $tutoURL; ?>/assets/js/tuto3.js'></script>
    <?php
    if ( strpos( $_SERVER['HTTP_HOST'], 'pixees.fr') !== FALSE ) {
        include '../shared/analytics.php';
    }
    ?>
</body>
</html>
