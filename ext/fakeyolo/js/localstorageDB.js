// ========================================================================
//  STORE data
// ========================================================================
var storeYoloJson="";


function storeData( theJson) {
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        localStorage.setItem("storeYoloJson", theJson);
    } else {
        // Sorry! No Web Storage support..
    }
}

function restoreJson() {
    theData="";
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        theData=localStorage.getItem("storeYoloJson");
        if(typeof theData != 'string') {
            theData="";
        }else{

        }
    } else {
        // Sorry! No Web Storage support..
        theData="notsupported";
    }
    return theData;
}

storeYoloJson=restoreJson();

