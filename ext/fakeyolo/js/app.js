let video;
let yolo;
let status;
let objects = [];

let vWidth = 0;
let vHeight = 0;
let modelLoaded = 0;
let interpretations = [];
let messageOn=0;

function setup() {
    video = createCapture(VIDEO);

    createCanvas(640, 480);
    //video.size(vWidth, vHeight);

    // Create a YOLO method
    yolo = ml5.YOLO(video, startDetecting);

    // Hide the original video
    video.hide();
    status = select('#status');
}

function draw() {

    if ((video.elt.width == 0) || (modelLoaded == 0)) {
        background("#FFFFFF");
        fill(0, 0, 0);
        textSize(24);
        textAlign(CENTER);
        text("Chargement en cours...", 0, 230, 640, 200);

    } else {
        if (vWidth == 0) {
            let prop = video.elt.height / video.elt.width;
            vWidth = 640;
            vHeight = Math.floor(640 * prop);
            textAlign(LEFT);
            detect();
        } else {
            // already set
        }
        image(video, 0, 0, vWidth, vHeight);
        if (typeof objects === 'undefined') {
            // pb
        }else{
            for (let i = 0; i < objects.length; i++) {
                let textWidth = 30;

                if (typeof robert['data'] === 'undefined') {
                    // robert pas encore chargé
                }else{
                        var objects_match = objects[i].label.toLowerCase();
                        interpretations[i]=objects[i].label;
                        for (var j = 0; j < robert['data'].length; j++) {
                            if (objects_match.indexOf(robert['data'][j].keyword[0].toLowerCase()) != -1) {
                                if(robert['data'][j].response[0]=="") {
                                    // on laisse interpretations[k]=objects[k].label
                                }else {
                                    interpretations[i]=robert['data'][j].response[0];
                                }
                            }else{
                                // pas la bonne interpretation
                            }
                        }
                }
                if (typeof interpretations[i] === 'undefined') {
                    interpretations[i]="...";
                }else{
                }
                textWidth = interpretations[i].length;
                noStroke();
                fill(0, 255, 0);
                rect(objects[i].x * vWidth, objects[i].y * vHeight, textWidth * 12, 30);
                fill(0, 0, 0);
                textSize(18);
                text(interpretations[i], objects[i].x * vWidth + 10, objects[i].y * vHeight + 20);
                noFill();
                strokeWeight(4);
                stroke(0, 255, 0);
                rect(objects[i].x * vWidth, objects[i].y * vHeight, objects[i].w * vWidth, objects[i].h * vHeight);
            }
            //-- communication avec le parent :
            if(messageOn==0) {
                messageOn=1;
                parent.postMessage("activeSuite", "*");
            }else{
            }

        }

    }


}

function startDetecting() {
    status.html('Model loaded!');
    status.remove();

    modelLoaded = 1;
}

function detect() {
    yolo.detect(function (err, results) {
        objects = results;
        if (typeof objects === 'undefined') {
            console.log("pb");
        }else{
            if (objects.length > 0) {

            } else {
// rine n'est détecté
            }

        }

        detect();
    });
}

//LOAD YOLO DATA
var robert;

$.ajax({
    cache: false,
    type: 'GET',
    url: 'data/fakeyolo.json',
    dataType: 'json',
    async: false,
    success: function (data) {
        if (storeYoloJson == "") {
            //robert = data;
            storeData(JSON.stringify(data));
            storeYoloJson = restoreJson();
        } else {
            //robert = JSON.parse(storeJson);
        }
        robert = JSON.parse(storeYoloJson);
        console.log(robert);
    }
});


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}


window.onmessage = function (e) {

    if(e.data=="toggle"){
        document.location="./admin/index.php";
    }else{
    }

}







