<?php

    //
    // Retourne un JSON détaillant le contenu des sous-dossiers ( pour chacun, la liste des images )
    //

    $imagesFolder = $_GET['folder'];
    $dir_path = dirname(getcwd(),2) . '/' . $imagesFolder;
    $path = '../../' . $imagesFolder;

    if (! is_dir($dir_path)) {
        header('Content-Type: application/json');
        echo json_encode("Unkwown folder");
        die();
    }

    // Retourne la liste des sous-dossiers du dossier passé en paramètre
    $folders = array_values(array_filter(scandir($path), function($folder) use ($path) {
        return is_dir($path . '/' . $folder) && (strlen($folder) > 2);
    }));

    $result = (object) array();

    $datasets = array();

    foreach($folders as $folder) {

        $folderpath = $path . '/' . $folder;

        $dataset = (object) array();
        $dataset->folder = $folder;
        $dataset->path = $imagesFolder . '/' . $folder;

        // Retourne la liste des fichiers du sous-dossier
        $files = array_values(array_filter(scandir($folderpath), function($file) use ($path) {
            return !is_dir($path . '/' . $file);
        }));

        $dataset_files = array();

        foreach($files as $file) {

            $dataset_file = (object) array();
            $dataset_file->name = $file;
            $dataset_file->path = $imagesFolder . '/' . $folder . '/' . $file;

            array_push($dataset_files, $dataset_file);
        }

        $dataset->files = $dataset_files;

        array_push($datasets, $dataset);
    }

    $result->datasets = $datasets;

    header('Content-Type: application/json');
    echo json_encode($result);

?>