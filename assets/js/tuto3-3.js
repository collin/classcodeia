(function($){

    // http://127.0.0.1:8080/edsa-websites/classcodeia/app/tuto3/experimenter/etape2
    // http://127.0.0.1:8080/edsa-websites/inria-tutos/app/test_images/

    // https://projects.invisionapp.com/share/ZMULMNT879R#/screens/394634306
    // https://docs.google.com/presentation/d/1yz2ib1BCuWb9aY3pAX0cWOII6e2S1UtkCkWzAg7xdpk/edit#slide=id.g70a7ca2b8c_0_1296

    // http://talkerscode.com/webtricks/preview-image-before-upload-using-javascript.php


    // Support pour le navigateur Microsoft Edge, version non-Chromium ( Microsoft Edge 44.18362.449.0 ):
    // 1. Un polyfill a été également ajouté pour le support de TextEncoding : https://github.com/inexorabletash/text-encoding
    // 2. Desactivation du passage par la carte graphique via WebGL
    if (document.documentMode || /Edge/.test(navigator.userAgent)) {
        _tfengine.ENV.set('WEBGL_PACK', false);
    }

    // Elements
    let $application = $('.tuto-ia-application');
    let $header  = $('#step-header');
    let $content = $('#step-contents');
    let $footer  = $('#step-footer');

    // BaseURL : écrit dans le index.php via PHP
    let baseURL = $application.attr('data-baseURL');

    // Lang : ecrit dans le index.php via PHP
    let lang = $application.attr('data-lang');

    // Chemins
    let dataFolder = baseURL + "/data";
    let assetsFolder = baseURL + "/assets";

    // Tutoriel 1 :
    let appBaseRoute = baseURL + "/app/tuto3-3";
    let tutorialFolder = dataFolder + "/tuto3-3";

    // Templates HandleBars :
    Handlebars.templateFolder = tutorialFolder + '/templates/';
    Handlebars.templates = [];

    // Mise en cache des templates JSON des différents écrans pour ne pas les charger plusieurs fois
    let _tutorialJson;

    let currentAbsoluteRoute;
    let currentRelativeRoute;

    let memoRelativeRoute="";

    let timeOutTuto;
    let indexStepTuto=1;
    let dureeTuto=2500;

    let timeOutRecord;
    let indexRecord=3;
    let widthSlider=0;


    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS LIEES AU TUTORIEL
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Evènements
    //

    // RQ : Les évènements souris sont détectés à la racine du DOM de l'application

    // Avant le chargement de la template :
    function beforeLoadingTemplateJsonForRoute( route ) {
        clearTimeout(timeOutTuto);
        indexStepTuto=1;

        let templateParams = {};

        route = route.split(appBaseRoute).join('');
        switch(route) {

            case '/':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "http://files.inria.fr/mecsci/classcodeIAI/tuto3/mp4/tuto3-activite3-vid1.mp4 ";
                templateParams.posterPath = tutorialFolder + "/medias/poster-neutre.jpg";//tutorialFolder + "/medias/vid1.jpg"; //!!!!!!!!!!!!!!!!!!!!!!!!!!! à remettre

                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto3-activite3-vid1.vtt";
                break;
            case '/comprendre/etape1':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "http://files.inria.fr/mecsci/classcodeIAI/tuto3/mp4/tuto3-activite3-vid2.mp4 ";
                templateParams.posterPath = tutorialFolder + "/medias/poster-neutre.jpg";//tutorialFolder + "/medias/vid1.jpg"; //!!!!!!!!!!!!!!!!!!!!!!!!!!! à remettre
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto3-activite3-vid2.vtt";
                break;
            case '/conclure/etape1':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "http://files.inria.fr/mecsci/classcodeIAI/tuto3/mp4/tuto3-activite3-vid3.mp4 ";
                templateParams.posterPath = tutorialFolder + "/medias/poster-neutre.jpg";//tutorialFolder + "/medias/vid1.jpg"; //!!!!!!!!!!!!!!!!!!!!!!!!!!! à remettre
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto3-activite3-vid3.vtt";
                break;

        }

        return templateParams;
    }

    // Une fois la template chargée et insérée dans le DOM :
    function afterInsertingTemplateForRoute( route ) {

        route = route.split(appBaseRoute).join('');

        console.log('Templated Loaded', route);

        let dataCompleteForCurrentStep;
        let infosCategory1, infosCategory2;
        $(".message-device").css('display','none');
        previousDisplay='none';

        switch(route) {

            case '/':
            case '/comprendre':
            case '/conclure':
                //-- menu -----------------------
                 $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../tuto3/" href="../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../tuto3-3/" href="/">> Le cri des animaux</a>')

                break;
            case '/comprendre/etape1':
                if(memoRelativeRoute=="/comprendre") {
                    // console.log($(".title-link.navigation-route").html());
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../tuto3/" href="../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../tuto3-3/" href="/">> Le cri des animaux</a>')

                }else{
                    //-- menu -----------------------
                    // console.log($(".title-link.navigation-route").html());
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-3/" href="/">> Le cri des animaux</a>')
                }
                break;

            case '/conclure/etape1':
                if(memoRelativeRoute=="/conclure") {
                    // console.log($(".title-link.navigation-route").html());
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../tuto3/" href="../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../tuto3-3/" href="/">> Le cri des animaux</a>')

                }else{
                    //-- menu -----------------------
                    // console.log($(".title-link.navigation-route").html());
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-3/" href="/">> Le cri des animaux</a>')
                }
                break;

            case '/roue':
                // visiblement impossible
                break;
            case '/roue/etape1':
                if(memoRelativeRoute=="/roue") {
                    $('.embiframe').attr('src','../../ext/roue/index.html');
                    //-- menu -----------------------
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../tuto3/" href="../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../tuto3-3/" href="/">> Le cri des animaux</a>')
                }else{
                    $('.embiframe').attr('src','../../../ext/roue/index.html');               //-- menu -----------------------
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-3/" href="/">> Le cri des animaux</a>')

                }
                $(".next-link .navigation-link").css('display','none');
               // console.log('non navigation-route next-link')
                if(chromeOK==false) {
                    $(".message-device").html('<p>Cette partie ne fonctionne entièrement que dans Chrome.</p><p>Veuillez quitter la page et changer de navigateur.</p>');
                    $(".message-device").css('display','block');
                    previousDisplay='block';
                }else{
                }
                break;

            case '/classifier':
                // visiblement impossible navigation-link
                break;
            case '/classifier/etape1':
                if(memoRelativeRoute=="/classifier") {
                    //-- menu -----------------------
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../tuto3/" href="../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../tuto3-3/" href="/">> Le cri des animaux</a>')
                }else{
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-3/" href="/">> Le cri des animaux</a>')
                }
                if(chromeOK==false) {
                    $(".message-device").html('<p>Cette partie ne fonctionne entièrement que dans Chrome.</p><p>Veuillez quitter la page et changer de navigateur.</p>');
                    $(".message-device").css('display','block');
                    previousDisplay='block';
                }else{
                }
                break;
            case '/classifier/etape2':
                if(memoRelativeRoute=="/classifier") {
                    $('.embiframe').attr('src','../../ext/animaux/index.html');
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../tuto3/" href="../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../tuto3-3/" href="/">> Le cri des animaux</a>')
                }else{
                    $('.embiframe').attr('src','../../../ext/animaux/index.html');
                    $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-3/" href="/">> Le cri des animaux</a>')

                }


                $(".next-link .navigation-link").css('display','none');
                if(chromeOK==false) {
                    $(".message-device").html('<p>Cette partie ne fonctionne entièrement que dans Chrome.</p><p>Veuillez quitter la page et changer de navigateur.</p>');
                    $(".message-device").css('display','block');
                    previousDisplay='block';
                }else{
                }
                break;
            case '/conclure/etape2':

                let contentHtml=$("#step-footer").html();
                contentHtml+='<ul class="navigation-next"><li class="navigation-route next-link next-chapter" ><a class="navigation-link forced" data-route="../../tuto3-4/" >Chapitre suivant</a></li></ul>';
                console.log(contentHtml);
                $("#step-footer").html(contentHtml);

                $(".title-link.navigation-route").html('<a class="navigation-link forced" data-route="../../tuto3/" href="../../tuto3/">Accueil</a><a class="navigation-link forced" data-route="../../tuto3-3/" href="/">> Le cri des animaux</a>')

                break;
            default:

                break;

        }
        //-- raz for all --------------

    }

    // Si l'utilisateur clique sur un élement d'interface
    $application.on('click', function(e) {

        let target = $(e.target);
        let message, $li;

        // Liens basiques (credits)
        if (target.hasClass("basic-link"))
        {
            return;
        }

        e.preventDefault();

        if (target.hasClass("forced"))
        {

            let navigationRoute = target.data('route');
            document.location=navigationRoute;
        }
        else if (target.hasClass("navigation-link"))
        {
            let navigationLi = target.closest('.navigation-route');
            let navigationRoute = navigationLi.data('route');


            $.router.set({
                route: appBaseRoute + navigationRoute
            });
        }
        else if (target.hasClass("video-parent"))
        {
            let videoElement = target.find('video');
            playVideo(videoElement);
        }
        else if ( target.hasClass("video-background") || target.hasClass("video-infos")) {

            let videoParent = target.closest('.video-parent');
            let videoElement = videoParent.find('video');
            playVideo(videoElement);

        }
        else if (target.hasClass("outer-link"))
        {
            window.open(target.attr("href"));
        }
        else if (target.hasClass("title-link"))
        {
            document.location=baseURL+"/app/tuto3/";
        }
        else if (target.hasClass("to_iframe"))
        {
            myIframe=document.getElementById('myiframe');
            myIframe.contentWindow.postMessage("lanceroue", '*');
        }
        else if (target.hasClass("previous-tuto"))
        {
            goRoute('/classifier/etape1');
        }
        else if (target.hasClass("credits-click"))
        {
            console.log("credits");
            goRoute('/');
        }

        
    });




    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS GENERALES
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Router
    // https://github.com/scssyworks/silkrouter/tree/feature/ver2
    //

    $.route(function (data) {

        currentAbsoluteRoute = data.route;

        let relativeRoute = currentAbsoluteRoute.split(appBaseRoute).join('');

        console.log("Route absolue", currentAbsoluteRoute, "Route relative", relativeRoute);
        memoRelativeRoute=relativeRoute;
        if (_tutorialJson === undefined) {
            loadChapitresJson( function( json ) {
                currentRelativeRoute = updateContentWithRoute( _tutorialJson = json, relativeRoute);
                console.log("currentRelativeRoute", currentRelativeRoute);
            });
        } else {
            currentRelativeRoute = updateContentWithRoute( _tutorialJson, relativeRoute);
        }
    });



    // init translations then init route
    initTutorielTranslations(baseURL, lang, tutorialFolder, function() {
        $.router.init();
    });



    // Retour forcé à l'accueil : par exemple, si l'utilisateur recharge une étape intermédiaire
    function backHome() {
        goRoute('/');
    }

    function goRoute( route ) {
        $.router.set({
            route: appBaseRoute + route
        });
    }


    //
    // Chargement du JSON des templates
    //

    function loadChapitresJson( successCallback ) {
        getTutorielJSON( tutorialFolder + '/json/chapitres.json', function( json ) {
            if(successCallback) {
                successCallback( json );
            }
        });
    }


    //
    // Affichage de la template de la route
    //

    function updateContentWithRoute( tutoJson, route ) {
        if(route=="/comprendre"){
            route="/comprendre/etape1";
        }else{
        }
        if(route=="/roue"){
            route="/roue/etape1";
        }else{
        }
        if(route=="/classifier"){
            route="/classifier/etape1";
        }else{
        }
        console.log("updateContentWithRoute", tutoJson, route);

        // La valeur de la route passée en paramètre peut être modifié ( ex : chapitre )
        let contentDescription = getStepDescriptionForRoute(tutoJson, route);
        let relativeRoute = contentDescription.step.route;

        //console.log("relativeRoute", contentDescription, relativeRoute);
        //console.log("appBaseRoute", appBaseRoute);

        // Préparation des données à injecter dans la template
        let templateParams = beforeLoadingTemplateJsonForRoute( relativeRoute );

        // Chargement de la template
        displayRoute( tutoJson, appBaseRoute, relativeRoute, $header, $content, $footer, contentDescription, templateParams );

        // Application des régles liés à la template après chargement
        afterInsertingTemplateForRoute(relativeRoute);

        return relativeRoute;
    }

    function playVideo(videoElement) {

        if (videoElement) {

            let videoParent = videoElement.closest('.video-parent');

            let video = videoElement[0];
            video.addEventListener('ended', endOfVideo, false);

            var textTracks = video.textTracks;
            if (textTracks && (textTracks.length > 0)) {
                var textTrack = textTracks[0];
                textTrack.mode = "showing";
            }

            if (video.paused === true) {
                video.play();
                videoParent.addClass('is-playing');
            } else {
                video.pause();
                videoParent.removeClass('is-playing');
            }
        }
    }

    function endOfVideo(e) {

        let video = e.target;
        if (video) {
            video.removeEventListener('ended', endOfVideo, false);
        }
        /*
        if ($('.navigation-route.next-link a').length) {
            $('.navigation-route.next-link a').trigger('click');
        } else if ($('.last-video-before-credits')) {
            $('.credits-link a').trigger('click');
        }
        */
    }

    function tutoStep(){

        //$('.explaination').css('display','none');
        //
        if(indexStepTuto<6) {
            $('.tuto-img img').css('display','none');
            indexStepTuto+=1;
            $('#explaination'+indexStepTuto).css('display','block');
            $('#img'+indexStepTuto).css('display','block');
            clearTimeout(timeOutTuto);
            timeOutTuto=setTimeout(tutoStep,dureeTuto);
        }else{
            //goRoute('/classifier/etape2');
            $(".next-link .navigation-link").html("Suite");
        }
    }

    window.onmessage = function (e) {
        console.log('reçu de top!!! :'+''+e.data);
        switch(e.data+''){

            case "relanceroue":
                $('.to_iframe').html('Relancez la roue');
                $(".next-link .navigation-link").css('display','block');
                break;


            case "consigne11":
                $('.consignes').html('Commençons par <span class="gras">le coq</span><br /><br />Nous avons besoin de 3 échantillons.<br /> <br /> Entrainez-vous à voix haute, et,\n' +
                    '        quand vous êtes prêt.e, cliquez sur "Record" et faites un beau cri de coq <span class="gras">durant toute la durée de l\'enregistrement</span>');
                break;
            case "consigne12":
                $('.consignes').html('Nous avons besoin de 2 autres échantillons.<br /> <br />Cliquez donc à nouveau sur "Record" et refaites le bruit du coq <span class="gras">pendant toute la durée de l\'enregistrement</span>');
                break;
            case "consigne13":
                $('.consignes').html('C\'est presque fini pour le coq.<br /> <br />Cliquez sur "Record" pour enregistrer le dernier échantillon et refaites votre plus beau cri.');
                break;
            case "consigne21":
                $('.consignes').html('Au cochon maintenant<br /><br />Nous avons besoin de 3 échantillons.<br /> <br /> \n' +
                    '        Quand vous êtes prêt.e, cliquez sur "Record" et grognez <span class="gras">durant toute la durée de l\'enregistrement</span>');
                break;
            case "consigne22":
                $('.consignes').html('Nous avons besoin de 2 autres échantillons.<br /> <br /> cliquez donc à nouveau sur "Record" et poussez un cri de cochon <span class="gras">pendant toute la durée de l\'enregistrement</span>');
                break;
            case "consigne23":
                $('.consignes').html('C\'est presque fini pour le cochon.<br /> <br /> cliquez sur "Record" pour enregistrer le dernier échantillon');
                break;
            case "consigne31":
                $('.consignes').html('Pour finir, il faut 3 échantillons du bruit de référence.<br /> <br /> Ne faites pas de bruit, et,\n' +
                    '        quand c\'est le plus silencieux possible, cliquez sur "Record"');
                break;
            case "consigne32":
                $('.consignes').html('Nous avons besoin de 2 autres échantillons.<br /> <br /> cliquez donc à nouveau sur "Record"');
                break;
            case "consigne33":
                $('.consignes').html('C\'est presque fini pour le son de référence.<br /> <br /> Cliquez sur "Record" pour enregistrer le dernier échantillon');
                break;
            case "consigneTrain":
                $('.consignes').html('Cliquez maintenant sur "Entraîner le modèle"...abracadabra...');
                break;
            case "finExperience":
                $('.consignes').html('Imitez le coq ou le cochon et voyez si l\'algorithme les reconnait<br /> Plus la barre est grande, plus il croit identifier le bruit correspondant.' );
                let currentHtml=$('#step-footer').html();
                $('#step-footer').html(currentHtml+ ' <a class="previous-tuto" href="/classifier/etape1">Refaire l\'expérience</a> ');

                $(".next-link .navigation-link").css('display','block');
                break;

            case "activeRecord":
                indexRecord=3;
                $('.decompte').css('display','block');
                $('.slide-out').css('display','block');
                $('.slide-in').css('display','block');
                $('.slide-in').css('width','0px');

                /*
                $('.decompte').html('Enregistrement dans<br /><span class="secondes"> '+indexRecord+' </span>sec.');
                clearTimeout(timeOutRecord);
                timeOutRecord=setTimeout(recCounter,1000);
                */

                // reprise de recCounter :
                indexRecord=0;
                widthSlider=0;
                $('.decompte').html('Enregistrement en cours : ');
                clearTimeout(timeOutRecord);
                requestAnimationFrame(step);

                break;

            default:
                console.log(e.data+'');
                break;
        }


/*
        if(e.data=="relanceroue"){
            $('.to_iframe').html('Relancez la roue');
        }else{
        }*/
    }

    function recCounter(){
        if(indexRecord>1){
            indexRecord-=1;
            $('.decompte').html('Enregistrement dans<br /><span class="secondes"> '+indexRecord+' </span>sec.');
            clearTimeout(timeOutRecord);
            timeOutRecord=setTimeout(recCounter,1000);
        }else{
            indexRecord=0;
            widthSlider=0;
            $('.decompte').html('Enregistrement en cours : ');
            clearTimeout(timeOutRecord);
            requestAnimationFrame(step);
        }

    }
    window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

    function step(timestamp) {
        let widthMax=parseInt($('.slide-out').css('width'));
        widthSlider+=1;
        $('.slide-in').css('width',widthSlider+'px');
        if(widthSlider>=widthMax){
            myIframe=document.getElementById('myiframe');
            myIframe.contentWindow.postMessage("saveSample", '*');
            $('.decompte').html('Enregistrement terminé');
            clearTimeout(timeOutRecord);
            timeOutRecord=setTimeout(recordEnd,1000);
        }else{
            requestAnimationFrame(step);
        }

    }
    function recordEnd(){
        $('.decompte').css('display','none');
        $('.slide-out').css('display','none');
        $('.slide-in').css('display','none');

    }


// On se rend à l'étape suivante
    //goRoute('/experimenter/etape3');
    //
    // Sélection d'images pour l'entrainement
    //


    //
    // Entités ML5
    //



    //
    // Webcam
    //


})( jQuery );
