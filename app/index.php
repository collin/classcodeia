<?php

$path = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);

if (!file_exists($path)) {

    $matches = array();
    preg_match('~tuto\d+[-]?[\d+]?~', $path, $matches);

    // Retourne : 'tuto1' ou 'tuto3-1'

    if ( count($matches) ) {
        include $matches[0] . "/index.php";
    }

} else {
    return false;
}

?>