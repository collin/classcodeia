(function($){

    // Algorithme de classification : (true), featureExtractor (false)
    const KNN = true;

    // documentation ML5 :
    // - K-Nearest neighbours Classifier : https://learn.ml5js.org/docs/#/reference/knn-classifier
    // - FeatureExtractor Classifier : https://learn.ml5js.org/docs/#/reference/feature-extractor

    // SOURCES KNN :
    // https://github.com/ml5js/ml5-library/blob/development/src/KNNClassifier/index.js


    // Support pour le navigateur Microsoft Edge, version non-Chromium ( Microsoft Edge 44.18362.449.0 ):
    // 1. Un polyfill a été également ajouté pour le support de TextEncoding : https://github.com/inexorabletash/text-encoding
    // 2. Desactivation du passage par la carte graphique via WebGL
    if (document.documentMode || /Edge/.test(navigator.userAgent)) {
        _tfengine.ENV.set('WEBGL_PACK', false);
    }

    // Elements
    let $application = $('.tuto-ia-application');
    let $header  = $('#step-header');
    let $content = $('#step-contents');
    let $footer  = $('#step-footer');
    let $dataSetElement;

    // BaseURL : écrit dans le index.php via PHP
    let baseURL = $application.attr('data-baseURL');

    // Lang : ecrit dans le index.php via PHP
    let lang = $application.attr('data-lang');

    // Chemins
    let dataFolder = baseURL + "/data";
    let datasetFolder = baseURL + "/datasets";
    let assetsFolder = baseURL + "/assets";

    // Tutoriel 1 :
    let appBaseRoute = baseURL + "/app/tuto1";
    let tutorialFolder = dataFolder + "/tuto1";

    // Templates HandleBars :
    Handlebars.templateFolder = tutorialFolder + '/templates/';
    Handlebars.templates = [];

    // Mise en cache des templates JSON des différents écrans pour ne pas les charger plusieurs fois
    let _tutorialJson;

    let currentAbsoluteRoute;
    let currentRelativeRoute;

    // Bibliothèque avec navigation
    let currentDatasetName;
    let currentDatasetCategory;

    // Nombre d'images dans la sélection
    const IMAGES_SELECTION_LENGTH = 10;
    const IMAGES_WEBCAM_LENGTH = 20;

    // Nombre d'images dans la librairie de test
    const IMAGES_PREDICTION_LENGTH = 40;

    // Mémorisation de sélection d'images (tableaux associatifs : clé = route relative )
    let _datasetSelections = [];
    let _datasetCategories = [];

    // Mémorisation des listes d'images de chaque catégorie  ( entrainement, prediction )
    let _datasetsJson;
    let _filteredDatasetsJson;

    // ML5 :
    let featureExtractor;
    let imageClassifier;

    // Webcam :
    let webcamVideo;



    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS LIEES AU TUTORIEL
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Evènements
    //

    // RQ : Les évènements souris sont détectés à la racine du DOM de l'application

    // Avant le chargement de la template :
    function beforeLoadingTemplateJsonForRoute( route ) {

        let templateParams = {};

        route = route.split(appBaseRoute).join('');
        switch(route) {

            case '/tester/etape1':
                closeWebcam();

                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto1/mp4/tuto1-activite1-vid1.mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto1-activite1-vid1.vtt";
                break;

            case '/tester/etape2':
                // Création d'un classifier d'images
                initPreTrainedClassifier();
                break;

            case '/experimenter/etape1':
                closeWebcam();

                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto1/mp4/tuto1-activite1-vid2.mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto1-activite1-vid2.vtt";
                break;

            case '/experimenter/etape2':
                // Catégorie 1 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/experimenter/etape2' );

                // Suppression des catégories et sélections d'images précédemment définies
                clearRouteData('/experimenter/etape2' );
                clearRouteData('/experimenter/etape3' );

                // Création d'un classifier d'images
                initFeaturesExtractionClassifier();
                break;

            case '/experimenter/etape3':
                // Catégorie 1 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/experimenter/etape2' );
                break;

            case '/experimenter/etape4':
                // Catégorie 1 et 2 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/experimenter/etape2' );
                templateParams.category2 = getImagesCategory( '/experimenter/etape3' );
                break;

            case '/experimenter/etape6':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto1/mp4/tuto1-activite1-vid3.mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto1-activite1-vid3.vtt";
                break;

            case '/experimenter/etape8':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto1/mp4/tuto1-activite1-vid4.mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto1-activite1-vid4.vtt";
                break;

            case '/experimenter/etape10':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto1/mp4/tuto1-activite1-vid5.mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto1-activite1-vid5.vtt";
                break;

            case '/creer/etape1':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto1/mp4/tuto1-activite1-vid6.mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto1-activite1-vid6.vtt";
                break;

            case '/creer/etape2':

                // Suppression des catégories et sélections d'images précédemment définies
                clearRouteData('/creer/etape2' );
                clearRouteData('/creer/etape3' );

                // Création d'un classifier d'images
                initFeaturesExtractionClassifier();
                break;

            case '/creer/etape4':
                // Catégorie 1 et 2 à insérer dans la template :
                templateParams.category1 = getImagesCategory( '/creer/etape2' );
                templateParams.category2 = getImagesCategory( '/creer/etape3' );
                break;

            case '/conclure/etape1':
                closeWebcam();

                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = "https://files.inria.fr/mecsci/classcodeIAI/tuto1/mp4/tuto1-activite1-vid7.mp4";
                templateParams.subtitlePath = tutorialFolder + "/vtt/tuto1-activite1-vid7.vtt";
                break;

            case '/conclure/etape2':
                // Chemin de la vidéo à insérer dans la template :
                templateParams.videoPath = tutorialFolder + "/medias/video1.mp4";
                break;

            case '/credits' :
                closeWebcam();
                break;

            case '/tester_entrainement' :
                // Création d'un classifier d'images
                initFeaturesExtractionClassifier();
                break;

            case '/tester_prediction' :
                // Création d'un classifier d'images
                initPreTrainedClassifier();
        }

        return templateParams;
    }

    // Une fois la template chargée et insérée dans le DOM :
    function afterInsertingTemplateForRoute( route ) {

        route = route.split(appBaseRoute).join('');

        // console.log('Templated Loaded', route);

        let dataCompleteForCurrentStep;
        let infosCategory1, infosCategory2;

        switch(route) {

            case '/':
               break;

            case '/tester/etape2':

                // Chargement de la bibliothèque (prediction)
                // On présente toutes les images de tous les sous-dossiers

                checkForNavigableDatasets(function (datasetsJson) {

                    // On fusionne tous les sous-dossiers dans un dataset :
                    let filteredData = [];
                    let filteredFiles = [];

                    let i, datasetsCount = datasetsJson.length, dataset;
                    for (i = 0; i < datasetsCount; i++) {

                        dataset = datasetsJson[i];

                        let j, m = dataset.files.length, fileInfo;
                        for (j = 0; j < m; j++) {
                            fileInfo = dataset.files[j];
                            filteredFiles.push({
                                name: fileInfo.name,
                                path: fileInfo.path,
                                datasetname: dataset.folder
                            });
                        }
                    }

                    filteredData.push({
                        files: filteredFiles
                    });

                    return filteredData;
                });

                break;

            case '/experimenter/etape2':

                // Initialisation de la sélection d'images de la première catégorie
                resetOrRestoreImagesSelection( '/experimenter/etape2' );

                // Chargement de la bibliothèque (entrainement) et du menu déroulant :
                checkForNavigableDatasets(null, function( datasetsOfCategoryJson ) {
                    // Remplissage du menu déroulant : liste des datasets de la catégorie courante ( entrainement )
                    updateDatasetSelector( $("#datasetsForCategory1") , datasetsOfCategoryJson);
                });

                break;

            case '/experimenter/etape3':

                dataCompleteForCurrentStep = false;

                // Affichage de la sélection d'images de la première catégorie
                if ( restoreImagesSelection( '/experimenter/etape2', $('.category1-images-grid') ))
                {
                    dataCompleteForCurrentStep = true;

                    // Affichage de la sélection d'images de la seconde catégorie
                    resetOrRestoreImagesSelection( '/experimenter/etape3' );

                    // Chargement de la bibliothèque (entrainement) et du menu déroulant :
                    checkForNavigableDatasets(null, function( datasetsOfCategoryJson ) {
                        // Remplissage du menu déroulant : liste des datasets de la catégorie courante ( entrainement )
                        updateDatasetSelector( $("#datasetsForCategory2") , datasetsOfCategoryJson);
                    });
                }

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour faire cette étape
                    // On renvoie vers l'accueil
                    backHome();
                }

                break;

            case '/experimenter/etape4':

                dataCompleteForCurrentStep = false;

                // Affichage de la sélection d'images de la première catégorie
                if ( restoreImagesSelection( '/experimenter/etape2', $('.category1-images-grid') ))
                {
                    // Affichage de la sélection d'images de la seconde catégorie
                    if ( restoreImagesSelection( '/experimenter/etape3', $('.category2-images-grid') ))
                    {
                        dataCompleteForCurrentStep = true;
                    }
                }

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();
                } else {
                    // Chargement de la bibliothèque :
                    checkForNavigableDatasets();
                }

                break;

            case '/experimenter/etape5':

                // Test1 : on ne présente que les deux catégories choisies ( dans entrainement )
                // en retirant les images sélectionnées

                infosCategory1 = getInfosFromStoredImageSelectionForRoute('/experimenter/etape2');
                infosCategory2 = getInfosFromStoredImageSelectionForRoute('/experimenter/etape3');
                dataCompleteForCurrentStep = infosCategory1&& infosCategory1.datasetName && infosCategory2 && infosCategory2.datasetName;

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();

                } else {

                    let selectedPaths = infosCategory1.paths.concat(infosCategory2.paths);

                    // Chargement de la bibliothèque (entrainement) :
                    checkForNavigableDatasets(function (datasetsJson) {

                        // On fusionne tous les sous-dossiers dans un dataset :
                        let filteredData = [];
                        let filteredFiles = [];

                        let i, datasetsCount = datasetsJson.length, dataset;
                        for (i = 0; i < datasetsCount; i++)
                        {
                            dataset = datasetsJson[i];
                            if ((dataset.folder === infosCategory1.datasetName) || (dataset.folder === infosCategory2.datasetName))
                            {
                                let j, m = dataset.files.length, fileInfo;
                                for (j = 0; j < m; j++) {
                                    fileInfo = dataset.files[j];
                                    if (selectedPaths.indexOf( fileInfo.path ) === -1) {
                                        filteredFiles.push({
                                            name: fileInfo.name,
                                            path: fileInfo.path,
                                            datasetname: dataset.folder
                                        });
                                    }
                                }
                            }
                        }

                        // On mélange et on ne conserve qu'un nombre limité d'images
                        filteredData.push({
                            files: shuffleAffay(filteredFiles).slice(0, IMAGES_PREDICTION_LENGTH)
                        });

                        return filteredData;
                    });

                }

                break;

            case '/experimenter/etape7':

                // Test2 : on présente les autres catégories ( dans entrainement ) que celles choisies

                infosCategory1 = getInfosFromStoredImageSelectionForRoute('/experimenter/etape2');
                infosCategory2 = getInfosFromStoredImageSelectionForRoute('/experimenter/etape3');
                dataCompleteForCurrentStep = infosCategory1&& infosCategory1.datasetName && infosCategory2 && infosCategory2.datasetName;

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();
                } else {

                    // Chargement de la bibliothèque (entrainement):
                    checkForNavigableDatasets(  function (datasetsJson) {

                        // On fusionne tous les sous-dossiers dans un dataset :
                        let filteredData = [];
                        let filteredFiles = [];

                        let i, datasetsCount = datasetsJson.length, dataset;

                        for (i = 0; i < datasetsCount; i++)
                        {
                            dataset = datasetsJson[i];
                            if ((dataset.folder !== infosCategory1.datasetName) && (dataset.folder !== infosCategory2.datasetName))
                            {
                                let j, m = dataset.files.length, fileInfo;
                                for (j = 0; j < m; j++) {
                                    fileInfo = dataset.files[j];
                                    filteredFiles.push({
                                        name: fileInfo.name,
                                        path: fileInfo.path,
                                        datasetname: dataset.folder
                                    });
                                }
                            }
                        }

                        // On mélange et on ne conserve qu'un nombre limité d'images
                        filteredData.push({
                            files: shuffleAffay(filteredFiles).slice(0, IMAGES_PREDICTION_LENGTH)
                        });

                        return filteredData;
                    });
                }

                break;

            case '/experimenter/etape9':

                // Test3 : on présente les deux catégories choisies ( mais dans prediction )

                infosCategory1 = getInfosFromStoredImageSelectionForRoute('/experimenter/etape2');
                infosCategory2 = getInfosFromStoredImageSelectionForRoute('/experimenter/etape3');
                dataCompleteForCurrentStep = infosCategory1 && infosCategory1.datasetName && infosCategory2 && infosCategory2.datasetName;

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();
                } else {

                    // Chargement de la bibliothèque (prédiction) :
                    checkForPredictionDatasets( infosCategory1.datasetName, infosCategory2.datasetName,function (datasetsJson) {

                        // On fusionne tous les sous-dossiers dans un dataset :
                        let filteredData = [];
                        let filteredFiles = [];

                        let i, datasetsCount = datasetsJson.length, dataset;

                        for (i = 0; i < datasetsCount; i++)
                        {
                            dataset = datasetsJson[i];
                            if ((dataset.folder === infosCategory1.datasetName) || (dataset.folder === infosCategory2.datasetName))
                            {
                                let j, m = dataset.files.length, fileInfo;
                                for (j = 0; j < m; j++) {
                                    fileInfo = dataset.files[j];
                                    filteredFiles.push({
                                        name: fileInfo.name,
                                        path: fileInfo.path,
                                        datasetname: dataset.folder
                                    });
                                }
                            }
                        }

                        // On mélange et on ne conserve qu'un nombre limité d'images
                        filteredData.push({
                            files: shuffleAffay(filteredFiles).slice(0, IMAGES_PREDICTION_LENGTH)
                        });

                        return filteredData;
                    });
                }

                break;

            case '/creer/etape2':

                // 1ère catégorie : constitution de la bibliothèque d'images de la webcam

                // Initialisation de la sélection d'images de la première catégorie
                resetOrRestoreImagesSelection( '/creer/etape2' );

                if (! webcamVideo )
                {
                    webcamVideo = initWebcam( function() {
                        $('.snapshot-button').removeClass('hidden');
                    });
                }
                else
                {
                    $('.snapshot-button').removeClass('hidden');
                    webcamVideo.play();
                }

                $('.parent-webcam').append( webcamVideo );

                break;

            case '/creer/etape3':

                // 2eme catégorie : constitution de la bibliothèque d'images de la webcam

                // Initialisation de la sélection d'images de la webcam
                resetOrRestoreImagesSelection( '/creer/etape3' );

                if (! webcamVideo )
                {
                    webcamVideo = initWebcam( function() {
                        $('.snapshot-button').removeClass('hidden');
                    });
                }
                else
                {
                    $('.snapshot-button').removeClass('hidden');
                    webcamVideo.play();
                }

                $('.parent-webcam').append( webcamVideo );


                break;

            case '/creer/etape4':

                dataCompleteForCurrentStep = false;

                // Affichage de la sélection d'images de la première catégorie
                if ( restoreImagesSelection( '/creer/etape2', $('.category1-images-grid') ))
                {
                    // Affichage de la sélection d'images de la seconde catégorie
                    if ( restoreImagesSelection( '/creer/etape3', $('.category2-images-grid') ))
                    {
                        dataCompleteForCurrentStep = true;
                    }
                }

                if (! dataCompleteForCurrentStep ) {
                    // Données incomplète pour pouvoir faire cette étape
                    // On renvoie vers l'accueil
                    backHome();
                } else {
                    // Chargement de la bibliothèque (prédiction) :
                    checkForNavigableDatasets();
                }

                break;

            case '/creer/etape5':

                // Test d'une image avec les images de la webcam :

                // Mise en place de la webcam dans la partie gauche de l'écran
                if (! webcamVideo )
                {
                    webcamVideo = initWebcam( function() {
                        $('.snapshot-button').removeClass('hidden');
                    });
                }
                else
                {
                    $('.snapshot-button').removeClass('hidden');
                    webcamVideo.play();
                }

                $('.parent-webcam').append( webcamVideo );

                break;

            case '/tester_entrainement' :

                checkForNavigableDatasets(function (datasetsJson) {

                    // On fusionne tous les sous-dossiers dans un dataset :
                    let filteredData = [];
                    let filteredFiles = [];

                    let i, datasetsCount = datasetsJson.length, dataset;
                    for (i = 0; i < datasetsCount; i++) {

                        dataset = datasetsJson[i];

                        let j, m = dataset.files.length, fileInfo;

                        for (j = 0; j < m; j++) {
                            fileInfo = dataset.files[j];
                            filteredFiles.push({
                                name: fileInfo.name,
                                path: fileInfo.path,
                                datasetname: dataset.folder
                            });
                        }
                    }

                    filteredData.push({
                        files: filteredFiles
                    });

                    return filteredData;
                });


                break;

            case '/tester_prediction' :

                // On présente toutes les images de tous les sous-dossiers
                checkForNavigableDatasets(function (datasetsJson) {

                    // On fusionne tous les sous-dossiers dans un dataset :
                    let filteredData = [];
                    let filteredFiles = [];

                    let i, datasetsCount = datasetsJson.length, dataset;
                    for (i = 0; i < datasetsCount; i++) {

                        dataset = datasetsJson[i];

                        let j, m = dataset.files.length, fileInfo;

                        for (j = 0; j < m; j++) {
                            fileInfo = dataset.files[j];
                            filteredFiles.push({
                                name: fileInfo.name,
                                path: fileInfo.path,
                                datasetname: dataset.folder
                            });
                        }
                    }

                    filteredData.push({
                        files: filteredFiles
                    });

                    return filteredData;
                });


                break;
        }
    }

    // Si l'utilisateur clique sur un élement d'interface
    $application.on('click', function(e) {

        let tagName = e.target.tagName.toLowerCase();
        let target = $(e.target);

        if ((tagName == "label") || (tagName == "input") || target.hasClass("checkmark"))  {

            // Cas des inputs, labels, ... où l'évènement doit conserver son fonctionnement normal
            if  (target.hasClass("checkmark"))
            {
                var liTarget = target.closest('.with-image-in-span');
                liTarget.removeClass().removeAttr('style').empty();

                if (liTarget.length &&  ! $('.validate-selection-button').hasClass("hidden")) {
                    $('.validate-selection-button').addClass("hidden");
                }
            }

        } else {

            // Sinon :

            // Liens basiques (credits)
            if (target.hasClass("basic-link"))
            {
                return;
            }

            e.preventDefault();

            let message, $li;

            if (target.hasClass("navigation-link"))
            {
                let navigationLi = target.closest('.navigation-route');
                let navigationRoute = navigationLi.data('route');

                $.router.set({
                    route: appBaseRoute + navigationRoute
                });
            }
            else if (target.hasClass("video-parent"))
            {
                let videoElement = target.find('video');
                playVideo(videoElement);
            }
            else if ( target.hasClass("video-background") || target.hasClass("video-infos")) {

                let videoParent = target.closest('.video-parent');
                let videoElement = videoParent.find('video');
                playVideo(videoElement);

            }
            else if (target.hasClass("previous-dataset"))
            {
                previousDataset();
            }
            else if (target.hasClass("next-dataset"))
            {
                nextDataset();
            }
            else if (target.hasClass("dataset-image"))
            {
                // Click sur une image de la bibliothèque :
                let imageTarget = $(e.target);

                // On vérifie que la bibliothèque a la propriété cliquable
                let clickableDataset = imageTarget.closest('.clickable-images-grid');
                if ( clickableDataset.length )
                {
                    //
                    // Cas de click sur une image d'une bibliothèque pour l'ajouter à la sélection
                    //

                    let parentOfImageForPrediction, added;

                    $('.dataset-image', clickableDataset).removeClass('last-selected');
                    imageTarget.addClass('last-selected');

                    switch(currentRelativeRoute) {

                        case '/experimenter/etape2' :
                        case '/experimenter/etape3' :

                            // Sélection d'image d'une même catégorie
                            added = addImageToSelection( imageTarget );

                            // On sauvegarde la catégorie de l'image ajoutée
                            saveImagesCategory( currentRelativeRoute, imageTarget.find('img').data('datasetname') );

                            // Si la sélection d'images est complète, on affiche le bouton :
                            if (added && isImagesSelectionComplete()) {
                                $('.validate-selection-button').removeClass('hidden');
                            }

                            break;

                        case '/tester/etape2' :
                        case '/experimenter/etape5' :
                        case '/experimenter/etape7' :
                        case '/experimenter/etape9' :
                        case '/tester_prediction'   :

                            // Sélection d'une seule image pour une prédiction

                            parentOfImageForPrediction = $('.image-for-prediction');
                            parentOfImageForPrediction.empty();

                            imageTarget.clone().appendTo( parentOfImageForPrediction );

                            $('.feature-predict-button').removeClass('hidden');
                            $('.prediction-result').empty();

                            break;

                    }
                }
                else if ( imageTarget.closest('.selected-images-grid').length )
                {
                    // Suppression d'une image du dataset

                    // On vérifie si cette image n'est pas Sélectionnée dans la bibliothèque
                    let imageURL = $('img', imageTarget).attr('src');
                    var gridElement = $('.clickable-images-grid');
                    $('li', gridElement).each(function(no, item) {
                        let $li = $(item);
                        if ($('img', $li).attr("src") === imageURL ) {
                            $li.removeClass("selected");
                        }
                    });

                    var liTarget = target.closest('.with-image-in-span');
                    if (liTarget.length === 0) {
                        liTarget = target;
                    }
                    liTarget.removeClass().removeAttr('style').empty();

                    // Bouton Valider
                    if (liTarget.length &&  ! $('.validate-selection-button').hasClass("hidden")) {
                        $('.validate-selection-button').addClass("hidden");
                    }
                }
            }
           else if  (target.hasClass("validate-selection-button"))
            {
                let categoryName;

                switch(currentRelativeRoute) {

                    case '/experimenter/etape2' :

                        // Validation de sélection d'images de la première catégorie

                        // On mémorise la sélection pour les écrans suivants ou pour un rechargement de la page
                        saveImagesSelection( currentRelativeRoute );

                        // On se rend à l'étape suivante
                        goRoute('/experimenter/etape3');

                        break;

                    case '/experimenter/etape3' :

                        // Validation de sélection d'images de la première catégorie

                        // On mémorise la sélection pour les écrans suivants ou pour un rechargement de la page
                        saveImagesSelection( currentRelativeRoute );

                        // On se rend à l'étape suivante
                        goRoute('/experimenter/etape4');

                        break;

                    case '/creer/etape2' :

                        // Validation de sélection d'images de la première catégorie

                        // Nom de la catégorie
                        categoryName = $('.category-input').val();
                        if (categoryName.length === 0) {
                            categoryName = "catégorie 1";
                        }

                        $('img', '.selected-images-grid').attr("data-datasetname", categoryName);

                        // On mémorise le nom de la catégorie
                        saveImagesCategory('/creer/etape2', categoryName );

                        $('li', '.dataset-images-grid').removeClass('with-checkbox');
                        $('label', '.dataset-images-grid li').remove();

                        // On mémorise la sélection pour les écrans suivants ou pour un rechargement de la page
                        saveImagesSelection( currentRelativeRoute );

                        // On se rend à l'étape suivante
                        goRoute('/creer/etape3');

                        break;

                    case '/creer/etape3' :

                        // Validation de sélection d'images de la première catégorie

                        // Nom de la catégorie
                        categoryName = $('.category-input').val();
                        if (categoryName.length === 0) {
                            categoryName = "catégorie 2";
                        }

                        $('img', '.selected-images-grid').attr("data-datasetname", categoryName);

                        // On mémorise le nom de la catégorie
                        saveImagesCategory('/creer/etape3', categoryName );

                        $('li', '.dataset-images-grid').removeClass('with-checkbox');
                        $('label', '.dataset-images-grid li').remove();

                        // On mémorise la sélection pour les écrans suivants ou pour un rechargement de la page
                        saveImagesSelection( currentRelativeRoute );

                        // On se rend à l'étape suivante
                        goRoute('/creer/etape4');

                        break;

                }
            }
            else if  (target.hasClass("train-selections-button"))
            {
                switch(currentRelativeRoute) {

                    case '/tester_entrainement' :

                        // On doit ajouter les images au classifier
                        addImagesToImagesClassifier( $('.dataset-images-grid img'), !KNN, function() {

                            imageClassifier.save('pretrained.json');

                        });

                    break;

                    case '/experimenter/etape4' :

                        // On doit ajouter les images au classifier
                        addImagesToImagesClassifier( $('.category-images-grid img'), !KNN, function() {

                            // On se rend à l'étape suivante
                            goRoute('/experimenter/etape5');

                        });

                        break;

                    case '/creer/etape4' :

                        // On doit ajouter les images au classifier
                        addImagesToImagesClassifier( $('.category-images-grid img'), !KNN, function() {

                            // On se rend à l'étape suivante
                            goRoute('/creer/etape5');

                        });

                        break;

                }
            }
            else if  (target.hasClass("feature-predict-button"))
            {
                $('.prediction-result').html('<p class="small">Analyse en cours...</p>');

                setTimeout( function() {

                    // Image à analyser :
                    let imageToPredict = $('.image-for-prediction').find('img')[0];

                    predictImageWithClassifier( imageToPredict,  function(err, results) {

                        // console.log(results);

                        if (err) {
                            message = "Erreur !";
                        } else if (results.length === 0) {
                            message = "Aucun résultat !";
                        } else {

                            let firstResult, secondResult;
                            let firstResultLabel, secondResultLabel;
                            let firstResultConfidence, secondResultConfidence;
                            let label;
                            let nbsp = String.fromCharCode(160);

                            if (KNN)
                            {
                                var confidencesForSorting = [];
                                for (label in results.confidencesByLabel) {
                                    confidencesForSorting.push(
                                        { label: label, confidence: Math.floor( results.confidencesByLabel[label] * 100) }
                                    );
                                }

                                confidencesForSorting.sort( function(a, b){
                                    return a.confidence > b.confidence ? -1 : +1;
                                });

                                message = "";

                                var i, n = confidencesForSorting.length, confidencesObj;
                                for(i=0;i<n;i++){
                                    confidencesObj = confidencesForSorting[i];
                                    if(confidencesObj.confidence > 0) {
                                        message += i === 0 ? '<p class="large">'  : '<p class="small">';
                                        message += confidencesObj.label + " : " + confidencesObj.confidence + nbsp + "%" + '</p>';
                                    }
                                }
                            }
                            else
                            {
                                firstResult = results[0];
                                firstResultLabel = firstResult.label;
                                firstResultConfidence = Math.floor( firstResult.confidence * 100);

                                secondResult = results[1];
                                secondResultLabel = secondResult.label;
                                secondResultConfidence = Math.floor( secondResult.confidence * 100);

                                message = "";
                                message += '<p class="large">' + firstResultLabel + " : " + firstResultConfidence + + nbsp + "%" + '</p>';
                                message += '<p class="small">' + secondResultLabel + " : " + secondResultConfidence + nbsp + "%" + '</p>';
                           }

                        }

                        $('.prediction-result').html( message );

                    });

                }, 100);

            }
            else if  (target.hasClass("snapshot-button"))
            {
                // Snapshot de la webcam pour créer la bibliothèque d'image

                switch(currentRelativeRoute) {
                    case '/creer/etape2' :
                    case '/creer/etape3' :
                        break;
                }

                $li = getVideoSnapShotWrappedAsLiWithCheckbox();
                addImageToSelection( $li );

                // Si la sélection d'images est complète, on affiche le bouton :
                if (isImagesSelectionComplete()) {
                    $('.validate-selection-button').removeClass('hidden');
                }

                // Sauvegarde pour les étapes suivantes
                saveImagesSelection(currentRelativeRoute);

            }
            else if  (target.hasClass("snapshot-for-test-button"))
            {
                // Snapshot de la webcam pour tester les deux catégories d'images issues de la webcam

                parentOfImageForPrediction = $('.image-for-prediction');
                parentOfImageForPrediction.empty();

                $li = getVideoSnapShotWrappedAsLi();
                $li.appendTo( parentOfImageForPrediction );

                $('.feature-predict-button').removeClass('hidden');

            }
        }
    });

    // Si l'utilisateur choisit une option d'un menu déroulant
    $application.on('change', function(e) {

        let tagName = e.target.tagName.toLowerCase();
        let target = $(e.target);

        if ((tagName == "label") || (tagName == "input"))  {
            // Cas des inputs, labels, ... où l'évènement doit conserver son fonctionnement normal

            var file = e.target.files[0];
            var imageType = /image.*/;

            if (file.type.match(imageType)) {

                var reader = new FileReader();

                if ( target.hasClass("dataset-file-upload"))
                {

                        reader.onload = function(e) {

                            var img = new Image();
                            img.src = reader.result;

                            let li = document.createElement('li');
                            li.appendChild(img);

                            let $li = $(li);
                            $li.addClass('with-image-in-span');
                            $li.addClass('with-checkbox');
                            $li.append('<span class="image">');

                            let $span = $li.find('span');
                            $span.addClass( 'dataset-image' );
                            $span.css('background-image', 'url(' +img.src+ ')' );

                            let $img  = $li.find('img');
                            $img.addClass("hidden-dataset-image");

                            // Checkbox
                            $li.append('<label class="styled-checkbox"><input type="checkbox" checked="checked" /><span class="checkmark"></span></label>');

                            var added = addImageToSelection( $li );

                            // Si la sélection d'images est complète, on affiche le bouton :
                            if (added && isImagesSelectionComplete()) {
                                $('.validate-selection-button').removeClass('hidden');
                            }
                        };

                        reader.readAsDataURL(file);

                }
                else if ( target.hasClass("prediction-file-upload"))
                {
                    reader.onload = function(e) {

                        var img = new Image();
                        img.src = reader.result;

                        let li = document.createElement('li');
                        li.appendChild(img);

                        let $li = $(li);
                        $li.addClass('with-image-in-span');
                        $li.append('<span class="image">');

                        let $span = $li.find('span');
                        $span.addClass( 'dataset-image' );
                        $span.css('background-image', 'url(' +img.src+ ')' );

                        let $img  = $li.find('img');
                        $img.addClass("hidden-dataset-image");

                        parentOfImageForPrediction = $('.image-for-prediction');
                        parentOfImageForPrediction.empty();

                        $li.appendTo( parentOfImageForPrediction );

                        $('.feature-predict-button').removeClass('hidden');

                    };

                    reader.readAsDataURL(file);
                }

            } else {
                // File not supported!
            }



        } else {

            // Sinon :

            e.preventDefault();

            if ( target.hasClass("dataset-names-selector"))
            {
                switch(currentRelativeRoute) {

                    case '/experimenter/etape2' :
                    case '/experimenter/etape3' :

                        // Expérimenter : sélection de la première bibliothèque
                        currentDatasetName = target.val();

                        // Chargement de la bibliothèque d'images choisie dans le selecteur
                        displayDataset( $dataSetElement, currentDatasetCategory, currentDatasetName );

                        // On vide la sélection d'images
                        resetImagesSelection();

                        break;
                }
            }
        }
    });

    // -------------------------------------------------------------------------------------------------------- //
    // FONCTIONS GENERALES
    // -------------------------------------------------------------------------------------------------------- //

    //
    // Router
    // https://github.com/scssyworks/silkrouter/tree/feature/ver2
    //

    $.route(function (data) {

        currentAbsoluteRoute = data.route;

        let relativeRoute = currentAbsoluteRoute.split(appBaseRoute).join('');

        // console.log("Route absolue", currentAbsoluteRoute, "Route relative", relativeRoute);

        if (_tutorialJson === undefined) {
            loadChapitresJson( function( json ) {
                currentRelativeRoute = updateContentWithRoute( _tutorialJson = json, relativeRoute);
                // console.log("currentRelativeRoute", currentRelativeRoute);
            });
        } else {
            currentRelativeRoute = updateContentWithRoute( _tutorialJson, relativeRoute);
        }
     });

    // init translations then init route
    initTutorielTranslations(baseURL, lang, tutorialFolder, function() {
        $.router.init();
    });



    // Retour forcé à l'accueil : par exemple, si l'utilisateur recharge une étape intermédiaire
    function backHome() {
        goRoute('/');
    }

    function goRoute( route ) {
        $.router.set({
            route: appBaseRoute + route
        });
    }


    //
    // Chargement du JSON des templates
    //

    function loadChapitresJson( successCallback ) {
        getTutorielJSON( tutorialFolder + '/json/chapitres.json', function( json ) {
            if(successCallback) {
                successCallback( json );
            }
        });
    }


    //
    // Affichage de la template de la route
    //

    function updateContentWithRoute( tutoJson, route ) {

        // console.log("updateContentWithRoute", tutoJson, route);

        // La valeur de la route passée en paramètre peut être modifié ( ex : chapitre )
        let contentDescription = getStepDescriptionForRoute(tutoJson, route);
        let relativeRoute = contentDescription.step.route;

        // console.log("relativeRoute", contentDescription, relativeRoute);

        // Préparation des données à injecter dans la template
        let templateParams = beforeLoadingTemplateJsonForRoute( relativeRoute );

        // Chargement de la template
        displayRoute( tutoJson, appBaseRoute, relativeRoute, $header, $content, $footer, contentDescription, templateParams );

        // Application des régles liés à la template après chargement
        afterInsertingTemplateForRoute(relativeRoute);

        return relativeRoute;
    }


    //
    // Datasets ( bibliothèque d'images )
    //

    // Template HTML d'une image : synchrone, définie dans la page index.php
    let datasetTemplateSource   = $("#dataset-template").html();
    let template = Handlebars.compile(datasetTemplateSource);

    // Dataset :
    function checkForNavigableDatasets( datasetFilterFunction, successCallback ) {

        $dataSetElement = $('.navigable-images-grid');
        if ( $dataSetElement.length )
        {
            let datasetCategory = $dataSetElement.data('category');
            if ((_datasetsJson === undefined) || (_datasetsJson[datasetCategory] === undefined)) {

                // console.log("Appel à l'API des images");

                // On interroge l'API pour récupérer la liste des sous-dossiers et leurs contenus pour une catégorie ( entrainement, prediction )
                loadJsonDataset( datasetCategory, function( json ) {

                    if (_datasetsJson === undefined) {
                        _datasetsJson = [];
                    }

                    // On stocke le retour de l'API pour un dossier ( entrainement, prediction)
                    _datasetsJson[datasetCategory] = json.datasets;

                    if (datasetFilterFunction) {
                        _filteredDatasetsJson = datasetFilterFunction( _datasetsJson[datasetCategory] );
                    } else {
                        _filteredDatasetsJson = _datasetsJson[datasetCategory];
                    }

                    // Premier affichage de la librairie du dossier ( entrainement, prediction )
                    displayDataset( $dataSetElement, datasetCategory );

                    if (successCallback) {
                        successCallback( _filteredDatasetsJson );
                    }
                });

            } else {

                // On a déjà téléchargé la liste des images du dossier ( entrainement, prediction )
                if (datasetFilterFunction) {
                    _filteredDatasetsJson = datasetFilterFunction( _datasetsJson[datasetCategory] );
                } else {
                    _filteredDatasetsJson = _datasetsJson[datasetCategory];
                }

                // Par défaut on ouvre la dernière catégorie affichée si elle existe dans le dossier ( entrainement, prediction )
                let datasetExists = getDatasetByName(datasetCategory, currentDatasetName);
                if (datasetExists) {
                    displayDataset( $dataSetElement, datasetCategory, currentDatasetName );
                } else {
                    displayDataset( $dataSetElement, datasetCategory );
                }

                if (successCallback) {
                    successCallback( _filteredDatasetsJson );
                }
            }
        }
    }

    function loadJsonDataset( datasetCategory, successCallback ) {
        let datasetsURL = datasetFolder + '/api/by_folder?folder=' + datasetCategory;
        $.ajax({
            url : datasetsURL,
            success : function(json) {
                if (successCallback) {
                    successCallback(json);
                }
            }
        });
    }

    // Dataset :
    function checkForPredictionDatasets( dataset1, dataset2, datasetFilterFunction, successCallback ) {

        $dataSetElement = $('.navigable-images-grid');
        if ($dataSetElement.length) {

            let datasetCategory = $dataSetElement.data('category');
            if ((_datasetsJson === undefined) || (_datasetsJson[datasetCategory] === undefined)) {

                // console.log("Appel à l'API des images de prédiction", datasetCategory, dataset1, dataset2);

                // On interroge l'API pour récupérer la liste des sous-dossiers et leurs contenus pour une catégorie ( entrainement, prediction )
                loadPredictionJsonDataset(datasetCategory, dataset1, dataset2, function (json) {

                    if (_datasetsJson === undefined) {
                        _datasetsJson = [];
                    }

                    // On stocke le retour de l'API pour un dossier ( entrainement, prediction)
                    _datasetsJson[datasetCategory] = json.datasets;

                    if (datasetFilterFunction) {
                        _filteredDatasetsJson = datasetFilterFunction(json.datasets);
                    } else {
                        _filteredDatasetsJson = json.datasets;
                    }

                    // Premier affichage de la librairie du dossier ( entrainement, prediction )
                    displayDataset($dataSetElement, datasetCategory);

                    if (successCallback) {
                        successCallback(_filteredDatasetsJson);
                    }
                });
            }
            else
            {
                // On a déjà chargé ce dataSet, on affiche les images sans télécharger les données JSON

                if (datasetFilterFunction) {
                    _filteredDatasetsJson = datasetFilterFunction( _datasetsJson[datasetCategory] );
                } else {
                    _filteredDatasetsJson = _datasetsJson[datasetCategory];
                }

                displayDataset($dataSetElement, datasetCategory);
            }
        }
    }

    function loadPredictionJsonDataset( datasetCategory, dataset1, dataset2, successCallback ) {
        let datasetsURL = datasetFolder + '/api/by_categories?folder=' + datasetCategory + '&dataset1=' + dataset1 + '&dataset2=' + dataset2;
        $.ajax({
            url : datasetsURL,
            success : function(json) {
                if (successCallback) {
                    successCallback(json);
                }
            }
        });
    }

    function previousDataset() {
        let datasetsJsonForFolder = _filteredDatasetsJson;
        let i, n = datasetsJsonForFolder.length, dataset;
        for(i=0;i<n;i++) {
            dataset = datasetsJsonForFolder[i];
            if (dataset.folder === currentDatasetName)
            {
                let previousDatasetIndex = i > 0 ? i - 1 : n - 1;
                let previousDataset = datasetsJsonForFolder[previousDatasetIndex];

                displayDataset( $dataSetElement, currentDatasetCategory, previousDataset.folder );
                break;
            }
        }
    }

    function nextDataset() {
        let datasetsJsonForFolder = _filteredDatasetsJson;
        let i, n = datasetsJsonForFolder.length, dataset;
        for(i=0;i<n;i++) {
            dataset = datasetsJsonForFolder[i];
            if (dataset.folder === currentDatasetName)
            {
                let nextDatasetIndex = i < n - 1 ? i + 1 : 0;
                let nextDataset = datasetsJsonForFolder[nextDatasetIndex];

                displayDataset( $dataSetElement, currentDatasetCategory, nextDataset.folder );
                break;
            }
        }
    }

    function getDatasetByName( datasetCategory, datasetName ) {
        let datasetsJsonForFolder = _filteredDatasetsJson;
        let i, n = datasetsJsonForFolder.length, dataset;
        for(i=0;i<n;i++) {
            dataset = datasetsJsonForFolder[i];
            if (dataset.folder === datasetName) {
                return dataset;
            }
        }

        // Par défaut
        return datasetsJsonForFolder[0];
    }

    function displayDataset( $divDataSet, datasetCategory, datasetName ) {

        $divDataSet.empty();

        let dataset = getDatasetByName( datasetCategory, datasetName );
        if (dataset)
        {
            currentDatasetName = dataset.folder;
            currentDatasetCategory = datasetCategory;

            let templateHTML = template( { datasetpath: datasetFolder, datasetname: currentDatasetName, files : dataset.files } );
            $divDataSet.append( templateHTML );
        }
    }


    function updateDatasetSelector( $select, datasetsOfCategoryJson ) {

        let selectHtml = '';
        selectHtml += '<option selected="selected">Sélectionner une catégorie</option>';

        let i, n = datasetsOfCategoryJson.length, categoryJson;
        for(i=0;i<n;i++) {
            categoryJson = datasetsOfCategoryJson[i];
            selectHtml += '<option>' + categoryJson.folder + '</option>';
        }

        $select.removeAttr("disabled");
        $select.html(selectHtml);


    }


    //
    // Sélection d'images pour l'entrainement
    //

    // Sauvegarde le HTML de la galerie d'images sélectionnées
    function saveImagesSelection( route ) {
        // console.log("saveImagesSelection", route);
        _datasetSelections[route] = $('.selected-images-grid').html();
    }

    function saveImagesCategory( route, category ) {
        _datasetCategories[route] = category;
    }

    function clearRouteData( route ) {

        if ( _datasetSelections ) {
            delete _datasetSelections[route];
        }

        if ( _datasetCategories ) {
            delete _datasetCategories[route];
        }
    }


    function getImagesCategory( route ) {
        return _datasetCategories[route];
    }

    function getStoredImageSelectionForRoute( route ) {
        return _datasetSelections[route];
    }

    function getInfosFromStoredImageSelectionForRoute( route ) {

        let imagesHtml = getStoredImageSelectionForRoute( route );
        let imagesNodes = $($.parseHTML(imagesHtml));
        let imagesPaths = [];
        let imageDatasetName;

        imagesNodes.find('img').each(function(no, item) {
            let itemElement = $(item);
            imagesPaths.push( itemElement.attr('data-path') );
            imageDatasetName = itemElement.attr('data-datasetname');
        });

        return { paths: imagesPaths, datasetName: imageDatasetName };
    }

    function resetOrRestoreImagesSelection( route, nbImages ) {

        // On est déjà passé par cette étape, on affiche ce qu'on avait choisi
        let storedSelection = getStoredImageSelectionForRoute( route );
        if ( storedSelection !== undefined )
        {
            $( '.selected-images-grid').html( storedSelection );
            return true;
        }

        // On a pas encore sélectionné ces images, on affiche les emplacements vides
        resetImagesSelection( nbImages );

        return false;
    }

    function restoreImagesSelection( route, $divElement ) {
        // console.log("restoreImagesSelection", route);

        let storedSelection = getStoredImageSelectionForRoute( route );
        if ( storedSelection !== undefined )
        {
            $divElement.html( storedSelection );
            return true;
        }

         return false;
    }

    function resetImagesSelection( nbImages ) {

        if ( isNaN(nbImages)) {
            nbImages = IMAGES_SELECTION_LENGTH;
        }

        let $divElement = $( '.selected-images-grid');
        let i;

        let ulHtml = '';

        ulHtml += '<ul>';

        for(i=0;i<nbImages;i++) {
            ulHtml += '<li></li>';
        }

        ulHtml += '</ul>';

        $divElement.html(ulHtml);
    }

    function isImagesSelectionComplete() {
        return getImagesSelectionCount() === IMAGES_SELECTION_LENGTH;
    }

    function isWebcamImagesComplete() {
        return getImagesSelectionCount() === IMAGES_WEBCAM_LENGTH;
    }

    function getImagesSelectionCount() {

        let $divElement = $( '.selected-images-grid');
        let nbImages = 0;

        $('li', $divElement).each(function(no, item) {
            let $li = $(item);
            if ( $li.children().length > 0 ) {
                nbImages++;
            }
        });

        return nbImages;
    }

    function addImageToSelection( $imageElement ) {

        // Dataset de l'image :
        let datasetImage = $imageElement.find('img').data('datasetname');

        // Est ce qu'elle appartient au même dataset ?
        let $datasetNamesSelector = $('.dataset-names-selector');
        let previousVal = $datasetNamesSelector.val();
        $datasetNamesSelector.val(datasetImage);

        if ( previousVal !== datasetImage ) {
            resetImagesSelection();
        }

        let $divElement = $( '.selected-images-grid');
        let imageAdded = false;

        // On vérifie si cette image n'est pas déjà présente
        let imageURL = $('img', $imageElement ).attr('src');
        let alreadySelected = false;
        let selectedImageCount = 0;

        $('li', $divElement).each(function(no, item) {
            let $li = $(item);

            let $imageItem = $('img', $li);
            if ($imageItem.length) {
                selectedImageCount++;
            }

            if ($imageItem.attr("src") === imageURL ) {
                alreadySelected = true;
                $li.trigger("click");
                $imageElement.removeClass("selected");
            }
        });

        if ( ! alreadySelected && (selectedImageCount < 10) ) {

            // On ajoute une coche sur l'image de la bibliothèque
            $imageElement.addClass('selected');

            // On cherche le premier <li> sans enfant pour le remplacer par celui sur lequel on a cliqué
            $('li', $divElement).each(function(no, item) {

                if ( ! imageAdded ) {
                    let $li = $(item);
                    if ( $li.children().length === 0 ) {
                        $li.replaceWith( $imageElement.clone().removeClass("selected") );
                        imageAdded = true;
                    }
                }
            });
        }

        return imageAdded;
    }

    //Cas particulier des images de la webcam

    function increaseWebcamImagesSelectionIfComplete() {
        let $divElement = $( '.selected-images-grid');
        if( getImagesSelectionCount() === $('li', $divElement).length )
        {
            $divElement.find('ul').append('<li></li>');
        }
    }


    //
    // Entités ML5
    //

    function initPreTrainedClassifier() {

        // Création d'un classifier d'images
        initFeaturesExtractionClassifier(function () {

            closeProgressPopUp();

            // Message d'attente
            openProgressPopUp("Chargement des données de pré-entrainement...");

            imageClassifier.load(datasetFolder + '/pretrained.json').then(function () {
                closeProgressPopUp();
            });
        });
    }

    function isImagesClassifierExists() {
        return imageClassifier !== undefined;
    }

    function initFeaturesExtractionClassifier( successCallback ) {

        if( successCallback === undefined ) {
            successCallback = modelReady;
        }

        // Message d'attente
        openProgressPopUp("Chargement du modèle...");

        if (KNN)
        {
            if (! featureExtractor || !imageClassifier )
            {
                // Modèle MobileNet + FeatureExtractor + KNN
                featureExtractor = ml5.featureExtractor("MobileNet", successCallback);
                imageClassifier = ml5.KNNClassifier();
            }
            else
            {
                // Si le KNN clasiifier existe déjà, on se contente de supprimer les résultats d'analyse des images
                imageClassifier.clearAllLabels();

                if(successCallback) successCallback();
            }
        }
        else
        {
            // Modèle MobileNet + FeatureExtractor
            featureExtractor = ml5.featureExtractor("MobileNet", successCallback);
            imageClassifier = featureExtractor.classification();
        }
    }

    function addImagesToImagesClassifier( $images, withTraining, successCallback ) {

        if (imageClassifier)
        {
            let imagesCount = $images.length;
            if (imagesCount > 0)
            {
                openProgressPopUp();

                displayProgressPopUp( "Ajout des images..." );

                let sequence = Promise.resolve();

                $images.each(function(no, image)
                {
                    sequence = sequence.then(function()
                    {
                        return new Promise(function(resolve, reject) {

                            // console.log("ADD IMAGE", no);

                            setTimeout(function ()
                            {
                                let $image = $(image);
                                let datasetName = $image.data('datasetname');
                                let classifierPromise;

                                    // En cas de succès :
                                let successFunction = function(result) {

                                    let pourcentage = Math.floor(100 * ((no + 1) / $images.length));
                                    displayProgressPopUp("Ajout des images : " + pourcentage + "%");

                                    if (no === $images.length - 1) {
                                        closeProgressPopUp();
                                    }

                                    resolve();
                                };

                                // En cas d'erreur :
                                let errorFunction = function(err) {
                                    console.log(err);
                                 };


                                if (KNN)
                                {
                                    // K-Nearest neighbours
                                    const features = featureExtractor.infer( image );
                                    imageClassifier.addExample(features, datasetName);

                                    classifierPromise = Promise.resolve();
                                    classifierPromise.then( successFunction, errorFunction);

                                    // console.log(features.dataSync());
                                }
                                else
                                {
                                    // Feature extractor
                                    classifierPromise = imageClassifier.addImage( image, datasetName);
                                    classifierPromise.then( successFunction, errorFunction);
                                }

                            }, 100);
                        });
                    });
                });

                sequence.then(function()
                {
                    return new Promise(function(resolve, reject) {

                        // Fermeture du popUp
                        closeProgressPopUp();

                        if (withTraining) {
                            trainFeaturesExtractionClassifier( successCallback );
                        } else {
                            successCallback();
                        }

                        resolve();
                    });
                });

            }
            else
            {
                console.log("Pas d'images sélectionnées à ajouter au modèle");
            }
        }
        else
        {
            console.log("Pas de classifier");
        }
    }

    // RQ : cette opération n'existe pas pour la méthode KNN ( K-Nearest neighbours )
    function trainFeaturesExtractionClassifier( successCallback ) {

        if (imageClassifier)
        {
            openProgressPopUp();
            displayProgressPopUp( "Entrainement du modèle en cours..." );

            imageClassifier.train(function(lossValue) {

                // console.log(lossValue);

                if (lossValue === null) {

                    closeProgressPopUp();

                    if (successCallback) {
                        successCallback();
                    }
                }
            });
        }
    }

    function predictImageWithClassifier( image, errorSuccessCallback ) {

        if (KNN)
        {
            // K-nearest neightbours
            const features = featureExtractor.infer(image);
            imageClassifier.classify(features, errorSuccessCallback );
        }
        else
        {
            // Feature extractor
            imageClassifier.classify(image, errorSuccessCallback);
        }
    }

    function modelReady() {
        closeProgressPopUp();
    }


    //
    // Popup
    //

    function openProgressPopUp( message ) {

        let popup = $('.popup');
        popup.removeClass('hidden');
        popup.append('<div class="popup-content"></div>');

        if (message) {
            displayProgressPopUp(message);
        }
    }

    function closeProgressPopUp() {
        let popup = $('.popup');
        popup.addClass('hidden');
        popup.empty();

    }

    function displayProgressPopUp( message ) {
        let popupContent = $('.popup-content');
        popupContent.empty();
        popupContent.append( '<p>' + message + '</p>' );
    }

    //
    // Webcam
    //

    function initWebcam( successCallback ) {

        let video;

        // Create video element that will contain the webcam image
        video = document.createElement('video');
        video.setAttribute('autoplay', '');
        video.setAttribute('playsinline', '');

        // Setup webcam
        navigator.mediaDevices.getUserMedia({ video: true, audio: false })
            .then((stream) => {
                video.srcObject = stream;
                video.width = 250;
                video.height = 184;

                video.addEventListener('playing', webcamIsPlaying);
                video.addEventListener('paused' , webcamIsNotPlaying);

                if (successCallback) {
                    successCallback();
                }
            });

        return video;
    }

    function closeWebcam() {
        if (webcamVideo) {
            webcamVideo.removeEventListener('playing', webcamIsPlaying);
            webcamVideo.removeEventListener('paused', webcamIsNotPlaying);
            webcamVideo.pause();

            let webcamStream = webcamVideo.srcObject;
            if (webcamStream) {
                webcamStream.getTracks()[0].stop();
            }

            webcamVideo.src = "";
        }

        webcamVideo = null;
    }

    function webcamIsPlaying() {
        videoPlaying = true;
    }

    function webcamIsNotPlaying() {
        videoPlaying = false;
    }

    function getVideoSnapShotWrappedAsLiWithCheckbox() {

        var $li  = getVideoSnapShotWrappedAsLi();
        $li.addClass('with-checkbox');
        $li.append('<label class="styled-checkbox"><input type="checkbox" checked="checked" /><span class="checkmark"></span></label>');

        return $li;
    }


    function getVideoSnapShotWrappedAsLi() {

        let img = getVideoSnapShot();

        let li = document.createElement('li');
        li.appendChild(img);

        let $li = $(li);
        $li.addClass('with-image-in-span');
        $li.append('<span class="image">');

        let $span = $li.find('span');
        $span.addClass( 'dataset-image' );
        $span.css('background-image', 'url(' +img.src+ ')' );

        let $img  = $li.find('img');
        $img.addClass("hidden-dataset-image");

        return $li;
    }

    function getVideoSnapShot() {

        let videoElement = $('video')[0];
        let canvas = document.createElement('canvas');
        canvas.height = videoElement.videoHeight;
        canvas.width = videoElement.videoWidth;

        let ctx = canvas.getContext('2d');
        ctx.drawImage(videoElement, 0, 0, canvas.width, canvas.height);

        let img = new Image();
        img.src = canvas.toDataURL();

        return img;
    }

    function playVideo(videoElement) {

        if (videoElement) {

            let videoParent = videoElement.closest('.video-parent');

            let video = videoElement[0];
            video.addEventListener('ended', endOfVideo, false);

            var textTracks = video.textTracks;
            if (textTracks && (textTracks.length > 0)) {
                var textTrack = textTracks[0];
                textTrack.mode = "showing";
            }

            if (video.paused === true) {
                video.play();
                videoParent.addClass('is-playing');
            } else {
                video.pause();
                videoParent.removeClass('is-playing');
            }
        }
    }

    function endOfVideo(e) {

        let video = e.target;
        if (video) {
            video.removeEventListener('ended', endOfVideo, false);
        }

        if ($('.navigation-route.next-link a').length) {
            $('.navigation-route.next-link a').trigger('click');
        } else if ($('.last-video-before-credits')) {
            $('.credits-link a').trigger('click');
        }
    }


})( jQuery );
