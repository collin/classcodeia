let video;
//let serial;
//let portName = '/dev/tty.usbmodem1421'; // fill in your serial port name here

// Create a KNN classifier
const knnClassifier = ml5.KNNClassifier();
let featureExtractor;

var class1IsActive = false;
var class2IsActive = false;
var class3IsActive = false;
var allowRecord = false;

var currentClass=1;
var currentSample=1;
//var ArduinoToggleLED = false;

function setup() {
  featureExtractor = ml5.featureExtractor('MobileNet', modelReady);
  //noCanvas();

  createButtons(); //ICI nous pouvons changer les class

  //serial = new p5.SerialPort();
  //serial.list(); //debug
  //serial.open(portName);
}

function modelReady(){
  //select('#status').html('FeatureExtractor(mobileNet model) Loaded')
  select('#status').html('mobileNet chargé');
    select('#consigne').html('Le modèle est chargé : Veuillez cliquer sur le bouton "start" pour lancer le spectrogramme');
    document.getElementById('buttonStart').style.display='block';
    allowRecord=true;

}

function addExample(label) {
  var dt = document.getElementById('mainCanvas');//document.body.querySelector('canvas');
    console.log("ouiiiii");
  const features = featureExtractor.infer(dt);
  knnClassifier.addExample(features, label);
  updateCounts();
}

// Predict the current frame.
function classify() {
  // Get the total number of labels from knnClassifier
  const numLabels = knnClassifier.getNumLabels();
  if (numLabels <= 0) {
    console.error('There is no examples in any label');
    return;
  }
  // Get the features of the input video
  var dt = document.getElementById('mainCanvas');//document.body.querySelector('canvas');
  const features = featureExtractor.infer(dt);

  // Use knnClassifier to classify which label do these features belong to
  // You can pass in a callback function `gotResults` to knnClassifier.classify function
  knnClassifier.classify(features, gotResults);
  // You can also pass in an optional K value, K default to 3
  // knnClassifier.classify(features, 3, gotResults);

  // You can also use the following async/await function to call knnClassifier.classify
  // Remember to add `async` before `function predictClass()`
  // const res = await knnClassifier.classify(features);
  // gotResults(null, res);
}

function draw() {
	if(class1IsActive == true){
		addExample('Hello');
        class1IsActive=false;
        //document.getElementById('addClassRock').style.backgroundColor='dodgerblue';
        //document.getElementById('addClassRock').style.color='#FFF';

        document.getElementById('ech1'+currentSample).style.borderWidth='1px';
        document.getElementById('ech1'+currentSample).style.borderColor='#AAA';
        memoCanvas();

        if(currentSample<3) {
            currentSample+=1;
        }else{
            currentClass+=1;
            currentSample=1;
        }
        setTimeout(nextConsigne,1000);
    }

	if(class2IsActive == true){
		addExample('Datadada');
        class2IsActive=false;
        //document.getElementById('addClassPaper').style.backgroundColor='dodgerblue';
        //document.getElementById('addClassPaper').style.color='#FFF';
        document.getElementById('ech2'+currentSample).style.borderWidth='1px';
        document.getElementById('ech2'+currentSample).style.borderColor='#AAA';
        memoCanvas();

        if(currentSample<3) {
            currentSample+=1;
        }else{
            currentClass+=1;
            currentSample=1;
        }

        setTimeout(nextConsigne,1000);
	}

	if(class3IsActive == true){
		addExample('Neutre');
        class3IsActive=false;
        //document.getElementById('addClassScissor').style.backgroundColor='dodgerblue';
        //document.getElementById('addClassScissor').style.color='#FFF';

        document.getElementById('ech3'+currentSample).style.borderWidth='1px';
        document.getElementById('ech3'+currentSample).style.borderColor='#AAA';
        memoCanvas();
        if(currentSample<3) {
            currentSample+=1;
            setTimeout(nextConsigne,1000);
        }else{

            setTimeout(trainAppears,1000);
            //currentClass+=1;
            //currentSample=1;
        }
	}
}
function nextConsigne(){
    document.getElementById('ech'+currentClass+""+currentSample).style.borderWidth='2px';
    document.getElementById('ech'+currentClass+""+currentSample).style.borderColor='#D2553F';
    document.getElementById('buttonRecord').style.display='block';
    parent.postMessage("consigne"+currentClass+""+currentSample, "*");
}
function trainAppears(){
    document.getElementById('buttonTrain').style.display='block';
    parent.postMessage("consigneTrain", "*");
}
function memoCanvas(){
    //console.log();
    var backCanv = document.getElementById('canv'+currentClass+""+currentSample);
    backCanv.width=60;
    backCanv.height=4096;
    var backCtx = backCanv.getContext('2d');
    backCtx.drawImage(document.getElementById('mainCanvas'), 0,0);

}
function createButtons() {
    buttonRec = select('#buttonRecord');
    buttonRec.mousePressed(function() {
        document.getElementById('buttonRecord').style.display='none';
        parent.postMessage("activeRecord", "*");
    });
    buttonTrain = select('#buttonTrain');
    buttonTrain.mousePressed(function() {
        document.getElementById('buttonTrain').style.display='none';
        document.getElementById('loadingfake').style.display='block';
        setTimeout(lastStep,2000);
    });
    buttonRec.mouseReleased(function() {
        //class1IsActive = false;
        //  document.getElementById('addClassRock').style.backgroundColor='dodgerblue';
        //  document.getElementById('addClassRock').style.color='#FFF';
    });
    /*
  //CLASS #1
  buttonA = select('#addClassRock');
  buttonA.mousePressed(function() {
    if((allowRecord==true)&&(started==true)) {
        document.getElementById('addClassRock').style.backgroundColor='#FF0';
        document.getElementById('addClassRock').style.color='#000';
        //class1IsActive = true;
        //-- communication avec le parent :
        currentClass=1;
        parent.postMessage("activeRecord", "*");
    }else{

    }

  });
  buttonA.mouseReleased(function() {
    //class1IsActive = false;
    //  document.getElementById('addClassRock').style.backgroundColor='dodgerblue';
    //  document.getElementById('addClassRock').style.color='#FFF';
  });

  //CLASS #2
  buttonB = select('#addClassPaper');
  buttonB.mousePressed(function() {
      if((allowRecord==true)&&(started==true)) {
          document.getElementById('addClassPaper').style.backgroundColor='#FF0';
          document.getElementById('addClassPaper').style.color='#000';
          //class2IsActive = true;

          //-- communication avec le parent :
          currentClass=2;
          parent.postMessage("activeRecord", "*");
      }else{

      }

  });
  buttonB.mouseReleased(function() {
   // class2IsActive = false;
   //   document.getElementById('addClassPaper').style.backgroundColor='dodgerblue';
   //   document.getElementById('addClassPaper').style.color='#FFF';
  });

  //CLASS #3
  buttonC = select('#addClassScissor');
  buttonC.mousePressed(function() {
      if((allowRecord==true)&&(started==true)) {
          document.getElementById('addClassScissor').style.backgroundColor='#FF0';
          document.getElementById('addClassScissor').style.color='#000';
          //class3IsActive = true;
          //-- communication avec le parent :
          currentClass=3;
          parent.postMessage("activeRecord", "*");
      }else{

      }

  });
  buttonC.mouseReleased(function() {
    //class3IsActive = false;
    //  document.getElementById('addClassScissor').style.backgroundColor='dodgerblue';
     // document.getElementById('addClassScissor').style.color='#FFF';
  });

  // Reset buttons
  resetBtnA = select('#resetRock');
  resetBtnA.mousePressed(function() {
      if((allowRecord==true)&&(started==true)) {
          clearLabel('Hello');
      }else{
      }

  });
	
  resetBtnB = select('#resetPaper');
  resetBtnB.mousePressed(function() {
      if((allowRecord==true)&&(started==true)) {
          clearLabel('Datadada');
      }else{
      }

  });
	
  resetBtnC = select('#resetScissor');
  resetBtnC.mousePressed(function() {
      if((allowRecord==true)&&(started==true)) {
          clearLabel('Neutre');
      }else{
      }

  });
*/
  // Predict button
  buttonPredict = select('#buttonPredict');
  buttonPredict.mousePressed(function() {
      if((allowRecord==true)&&(started==true)) {
          document.getElementById('buttonPredict').style.backgroundColor='#FF0';
          document.getElementById('buttonPredict').style.color='#000';
          classify();
      }else{
      }

  });
    buttonPredict.mouseReleased(function() {
        document.getElementById('buttonPredict').style.backgroundColor='dodgerblue';
        document.getElementById('buttonPredict').style.color='#FFF';
    });


  // Clear all classes button
  /*buttonClearAll = select('#clearAll');
  buttonClearAll.mousePressed(clearAllLabels);*/

  // Load saved classifier dataset
  //buttonSetData = select('#load');
  //buttonSetData.mousePressed(loadMyKNN);

  // Get classifier dataset
  //buttonGetData = select('#save');
  //buttonGetData.mousePressed(saveMyKNN);
}
function lastStep(){
    document.getElementById('loadingfake').style.display='none';
    document.getElementById('class_output').style.display='block';

    parent.postMessage("finExperience", "*");
    classify();
}
// Show the results
function gotResults(err, result) {
  // Display any error
  if (err) {
    console.error(err);
  }

  if (result.confidencesByLabel) {
    const confidences = result.confidencesByLabel;
    // result.label is the label that has the highest confidence
    if (result.label) {
      select('#result').html(result.label);
      select('#confidence').html(`${confidences[result.label] * 100} %`);
    }

    select('#confidenceRock').value(`${confidences['Hello'] ? confidences['Hello'] * 100 : 0}`);
    select('#confidencePaper').value(`${confidences['Datadada'] ? confidences['Datadada'] * 100 : 0}`);
    select('#confidenceScissor').value(`${confidences['Neutre'] ? confidences['Neutre'] * 100 : 0}`);
/*
    if(confidences['Hello'] > 0.9 && ArduinoToggleLED == false){
      //serial.write('P');
      console.log("Turn On the LED [P]");
      ArduinoToggleLED = true;
    }

    if(confidences['Datadada'] > 0.9 && ArduinoToggleLED == true){
      //serial.write('0');
      console.log("Turn Off the LED [0]");
      ArduinoToggleLED = false;
    }
 */

  }

  classify();
}

// Update the example count for each label	
function updateCounts() {
  const counts = knnClassifier.getCountByLabel();

  select('#exampleRock').html(counts['Hello'] || 0);
  select('#examplePaper').html(counts['Datadada'] || 0);
  select('#exampleScissor').html(counts['Neutre'] || 0);
}

// Clear the examples in one label
function clearLabel(label) {
  knnClassifier.clearLabel(label);
  updateCounts();
}

// Clear all the examples in all labels
/*function clearAllLabels() {
  knnClassifier.clearAllLabels();
  updateCounts();
}*/

// Save dataset as myKNNDataset.json
function saveMyKNN() {
  knnClassifier.save('myKNNDataset');
}

// Load dataset to the classifier
function loadMyKNN() {
  knnClassifier.load('./myKNNDataset.json', updateCounts);
}

function loadDataset(){
  var file = document.getElementById('load').files[0];
  var path = (window.URL || window.webkitURL).createObjectURL(file);
  console.log(file+" "+path);
  knnClassifier.load(path, updateCounts);
}

//console.clear();

//var count = 0;

// UPDATE: there is a problem in chrome with starting audio context
//  before a user gesture. This fixes it.
var started = null;


document.getElementById('buttonStart').addEventListener('click', () => {
  if(allowRecord==true) {
      if (started) {
        //already initialized
      }else{
          started = true;
          initialize();

          document.getElementById('buttonStart').style.display='none';
          nextConsigne();
      }
  }else{
    // model not ready
  }

})

function initialize() {
  console.log("initialize");
  document.body.querySelector('h3').remove();
  const CVS = document.getElementById('mainCanvas');//document.body.querySelector('canvas');
  const CTX = CVS.getContext('2d');
  const W = CVS.width = 60; //on rèlge ici la longeur du son
  const H = CVS.height = 4096;

  const ACTX = new AudioContext();
  const ANALYSER = ACTX.createAnalyser();

  ANALYSER.fftSize = 4096; //4096 //512  
  
  navigator.mediaDevices
  .getUserMedia({ audio: true })
  .then(process);

  function process(stream) {
    const SOURCE = ACTX.createMediaStreamSource(stream);
    SOURCE.connect(ANALYSER);
    const DATA = new Uint8Array(ANALYSER.frequencyBinCount);
    const LEN = DATA.length;
    const h = H / LEN;
    const x = W - 1;
    CTX.fillStyle = 'hsl(280, 100%, 10%)';
    CTX.fillRect(0, 0, W, H);

    loop();

    function loop() {   
      
      //if(count < 120){
      window.requestAnimationFrame(loop);
      //}

      //count++;

      let imgData = CTX.getImageData(1, 0, W - 1, H);
      CTX.fillRect(0, 0, W, H);
      CTX.putImageData(imgData, 0, 0);
      ANALYSER.getByteFrequencyData(DATA);
      for (let i = 0; i < LEN; i++) {
        let rat = DATA[i] / 255;
        let hue = Math.round((rat * 120) + 280 % 360);
        let sat = '100%';
        let lit = 10 + (70 * rat) + '%';
        CTX.beginPath();
        CTX.strokeStyle = `hsl(${hue}, ${sat}, ${lit})`;
        CTX.moveTo(x, H - (i * h));
        CTX.lineTo(x, H - (i * h + h));
        CTX.stroke();
      }
    }
  }
}

/*function download() {
    var dt = document.body.querySelector('canvas').toDataURL('image/jpeg');
    this.href = dt;
};
downloadLnk.addEventListener('click', download, false);*/


window.onmessage = function (e) {

    if(e.data=="saveSample"){
        switch(currentClass){
            case 1:
                class1IsActive=true;
                break;
            case 2:
                class2IsActive=true;
                break;
            case 3:
                class3IsActive=true;
                break;
        }
    }else{
    }

}

