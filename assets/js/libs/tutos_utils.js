function getTutorielJSON( filepath, successCallback ) {

    $.ajax({
        url : filepath,
        success : function(data) {
           if (successCallback) {
               successCallback( data );
            }
        },
        async : false
    });

}

function getStepDescriptionForRoute( tutoJson, route ) {
    // On cherche dans le JSON de  description de l'application, l'objet décrivant la route demandée
    return ((route === undefined) || (route === "/")) ? getFirstStepDescription(tutoJson) : getStepDescription(tutoJson, route);
}

function getFirstStepDescription( tutoJson ) {

    var chapters = tutoJson.chapters;
    var firstChapter = chapters[0];
    var firstStep = firstChapter.steps[0];

    if (tutoJson.home)
    {
        return {
            step: tutoJson.home,
            steps: [],
            next_route:firstStep.route
        };

    }
    else
    {
        return {
            step: tutoJson.home ? tutoJson.home : firstStep,
            steps: firstChapter.steps,
            active_chapter: 0,
            active_step: 0
        };
    }
}

function getStepDescription( tutoJson, route ) {

    var chapters = tutoJson.chapters;
    var i, n = chapters.length, chapterDescription;
    var j, m, steps, stepDescription;

    var result;

    for(i=0;i<n;i++) {

        chapterDescription = chapters[i];
        steps = chapterDescription.steps;
        m = steps.length;

        // On recherche si la route demandée est celle d'une étape
        for(j=0;j<m;j++) {
            stepDescription = steps[j];

            // Si on a déjà trouvé la route passée en paramètre dans une précédente boucle
            // on mémorise la route de l'étape suivante
            if (result && (result.next_route === undefined) && (chapterDescription.public !== false)) {
                result.next_route = stepDescription.route;
            }

            // On cherche la route passée en paramètre
            if ( stepDescription.route === route) {
                result = {
                    step : stepDescription,
                    steps: chapterDescription.steps,
                    active_chapter: i,
                    active_step: j
                };
            }

        }

        if (m>0) {
            // S'il y a des étapes, et que la route passée est celle du chapitre, on retourne
            // la description de la première étape du chapitre en question
            if ( route === chapterDescription.route ) {
                result = {
                    step : steps[0],
                    steps: chapterDescription.steps,
                    active_chapter: i,
                    active_step: 0,
                    next_route: steps.length > 1 ? steps[1].route : null
                };
            }
        }
    }

    return result === undefined ? false : result;
}

function displayRoute( tutoJson, appBaseRoute, route, $headerDiv, $contentDiv, $footerDiv, contentDescription, templateParams ) {

    // { step: étape correspondant à la route , steps : liste des étapes du chapitre de la route }

    if (contentDescription.step === undefined) {
        console.log("[Erreur] displayRoute", route, contentDescription);
        return;
    }

    var templateName = contentDescription.step.template;
    var next_button = contentDescription.step.next_button === false ? false : true;
    var next_route = next_button ? contentDescription.next_route : null;

    // Header : Chapitres
    displayChapters(tutoJson, appBaseRoute, $headerDiv, contentDescription.active_chapter);

    // Content : Page Template
    displayContent(templateName, templateParams, $contentDiv);

    // Footer : Etapes du chaitre courant
    displaySteps(tutoJson, contentDescription.steps, appBaseRoute, $footerDiv, contentDescription.active_step, next_route);
}

function displayChapters( tutoJson, appBaseRoute, $headerDiv, activeChapterIndex ) {

    var chapters = tutoJson.chapters;
    var i, n = chapters.length, chapterDescription;
    var headerHtml = '';
    var navigationRouteCssClass = "navigation-route";
    var chapterRoute, chapterCss;
    var homeRoute = "/";
    var pixeesURL = "https://pixees.fr/classcode-v2/iai/";
    var titleLinkCss = isNaN( activeChapterIndex) ? 'no-active-chapter' : '';

    // Permet de dire si on est dans le tuto ou dans l'accueil
    $('.tuto-ia-application').removeClass('no-active-chapter');
    if (isNaN( activeChapterIndex)) {
        $('.tuto-ia-application').addClass('no-active-chapter');
    }

    if (tutoJson.module) {
        pixeesURL += tutoJson.module;
    }

    // Accueil
    headerHtml += '<span data-route="' + homeRoute + '" class="' + navigationRouteCssClass + ' home-link">';
    headerHtml += '<a class="basic-link" href="' + pixeesURL + '" target="_blank">Accueil</a>';
    headerHtml += '</span>';

    // Chapitres
    headerHtml += '<ul class="navigation chapters">';

    for(i=0;i<n;i++)
    {
        chapterDescription = chapters[i];
        if ( chapterDescription.public != false ) {

            chapterCss = ( i === activeChapterIndex ? " active" : "");
            chapterRoute = appBaseRoute + chapterDescription.route;

            headerHtml += '<li data-route="' + chapterDescription.route + '" class="' + navigationRouteCssClass + chapterCss + '">';
            headerHtml += '<a class="navigation-link" href="' + chapterRoute + '">';
            headerHtml += i18n(chapterDescription.title);
            headerHtml += '</a></li>';

        }
    }

    headerHtml += '</ul>';

    // Titre
    headerHtml += '<span data-route="' + homeRoute + '" class="title-link ' + navigationRouteCssClass + ' ' + titleLinkCss + '">';
    headerHtml += '<a class="navigation-link" href="/">'+ i18n(tutoJson.html_title) +'</a>';
    headerHtml += '</span>';

    $headerDiv.empty();
    $headerDiv.html(headerHtml);
}

function displaySteps( tutoJson, stepsJson, appBaseRoute, $footerDiv, activeStepIndex, next_route ) {

    var i, n = stepsJson.length, stepDescription;
    var footerHtml = '';
    var navigationRouteCssClass = "navigation-route";
    var stepRoute, stepCss;
    var creditsRoute = '/credits';


    // Points (étapes)
    footerHtml += '<ul class="navigation step-navigation">';

    if (stepsJson.length > 0)
    {
        if (activeStepIndex === undefined) {
            activeStepIndex = 0;
        }

        stepDescription = stepsJson[activeStepIndex];
        if (stepDescription.navigation_bullet !== false)
        {
            for(i=0;i<n;i++)
            {
                stepDescription = stepsJson[i];

                if (stepDescription.navigation_bullet !== false) {
                    stepCss = ( i === activeStepIndex ? " active" : "");
                    stepRoute = appBaseRoute + stepDescription.route;

                    footerHtml += '<li data-route="' + stepDescription.route + '" class="' + navigationRouteCssClass + stepCss + '">';
                    footerHtml += '<a class="navigation-link" href="'  + stepRoute + '">' + (i+1) + '</a>';
                    footerHtml += '</li>';
                }
            }
        }
    }

    footerHtml += '</ul>';

    if (next_route) {
        footerHtml += '<ul class="navigation-next">';
        footerHtml += '<li class="'+ navigationRouteCssClass + ' next-link" data-route="' + next_route + '">';
        footerHtml += '<a class="navigation-link" href="'  + next_route + '">' + i18n("Suite") +'</a>';
        footerHtml += '</li>';
        footerHtml += '</ul>';
    }

    // Crédits
    footerHtml += '<span data-route="' + creditsRoute + '" class="credits-link ' + navigationRouteCssClass + '">';
    footerHtml += '<a class="navigation-link" href="' + creditsRoute + '">' + i18n("Crédits") +' • CC • BY • SA  2020</a>';
    footerHtml += '</span>';

    $footerDiv.empty();
    $footerDiv.html(footerHtml);

}

function displayContent( templateName, templateParams, $contentDiv ) {
    loadTemplateInElement( templateName, templateParams, $contentDiv );
}

function loadTemplateInElement ( templateName, templateParams, $div ) {

    $div.empty();

    var templateFolder = Handlebars.templateFolder;

    // console.log("loadTemplateInElement", templateName, "templateFolder", templateFolder);

    var template = Handlebars.getTemplate(templateFolder, templateName);
    if (template)
    {
        var templateHTML = template( templateParams );
        $div.append( templateHTML );
    }
}

function shuffleAffay (p_array)
{
    var n = p_array.length;
    var i = n;
    var temp;
    var p;

    while (i--) {
        p = Math.floor(Math.random()*n);
        temp = p_array[i];
        p_array[i] = p_array[p];
        p_array[p] = temp;
    }
    return p_array;
}

function initTutorielTranslations (baseURl, lang, tutorialFolder, callback) {
    // Get common translation
    $.ajax(`${baseURl}/assets/translations/${lang}.json`).always(function(data){
        if (!data.values) return;
        i18n.translator.add(data);
    })


    // Get tutoriel translation
    $.ajax(`${tutorialFolder}/translations/${lang}.json`).always(function(data){
        if (data.values) i18n.translator.add(data);
        callback();
    })

    // Register Handlebars helper to provide translations inside templates
    Handlebars.registerHelper('i18n', i18n);
}
